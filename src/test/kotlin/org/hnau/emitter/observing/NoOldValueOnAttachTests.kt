package org.hnau.emitter.observing

import org.assertj.core.api.Assertions.assertThat
import org.hnau.emitter.observing.push.always.content.ManualEmitter
import org.hnau.emitter.observing.push.lateinit.cache
import org.junit.Test


class NoOldValueOnAttachTests {

    @Test
    fun test() {

        val source = ManualEmitter(0)
        val cache = source.cache()
        val values = ArrayList<Int>()

        val observer: (Int) -> Unit = { value -> values.add(value) }

        val detacher = cache.observe(observer)

        detacher()
        source.value = 1

        cache.observe(observer)

        assertThat(values).isEqualTo(listOf(0, 1))

    }

}