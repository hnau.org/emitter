package org.hnau.emitter.observing

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


class ObservingEmitterTests {

    @Test
    fun testCallWhileCallHandling() {

        val emitter = ObservingEmitterSimple<Int>()

        val values1 = ArrayList<Int>()

        emitter.observe { value ->
            values1.add(value)
            if (value < 3) {
                emitter.call(value + 1)
            }
        }

        val values2 = ArrayList<Int>()
        emitter.observe { value -> values2.add(value) }

        emitter.call(0)

        assertThat(values1).isEqualTo(listOf(0, 1, 2, 3))
        assertThat(values2).isEqualTo(listOf(3))


    }

}