package org.hnau.emitter


object EmptyEmitter : Emitter<Nothing> {

    override fun observe(observer: (Nothing) -> Unit) =
            EmitterDetachers.empty

}

fun <T> Emitter.Companion.empty(): Emitter<T> = EmptyEmitter