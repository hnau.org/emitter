package org.hnau.emitter

typealias EmitterDetacher = () -> Unit

object EmitterDetachers {

    val empty: EmitterDetacher = {}

}