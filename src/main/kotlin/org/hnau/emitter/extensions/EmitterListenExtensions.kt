package org.hnau.emitter.extensions

import org.hnau.emitter.Emitter


inline fun <T> Emitter<T>.listen(
        crossinline observer: () -> Unit
) = observe { _ ->
    observer()
}