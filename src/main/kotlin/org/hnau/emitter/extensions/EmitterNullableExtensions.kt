package org.hnau.emitter.extensions

import org.hnau.emitter.Emitter


fun <T> Emitter<T>.callIfNull() = callIf { it == null }
fun <T> Emitter<T>.callIfNotNull() = callIf { it != null }

fun <T> Emitter<T>.mapIsNull() = map { it == null }.unique()
fun <T> Emitter<T>.mapIsNotNull() = map { it != null }.unique()