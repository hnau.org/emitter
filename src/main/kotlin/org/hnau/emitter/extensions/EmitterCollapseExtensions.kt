package org.hnau.emitter.extensions

import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.lateinit.collapse
import org.hnau.emitter.empty


fun <T> Emitter<T>.useWhen(
        isNeedUse: Emitter<Boolean>,
        placeholder: Emitter<T> = Emitter.empty<T>()
) = isNeedUse.map { needUse ->
    needUse.checkBoolean(
            ifTrue = { this@useWhen },
            ifFalse = { placeholder }
    )
}.collapse()
