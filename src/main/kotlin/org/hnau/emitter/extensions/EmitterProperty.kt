package org.hnau.android.base.utils.preferences.property

import org.hnau.base.data.delegate.mutable.MutableDelegate
import org.hnau.base.data.possible.box.Box
import org.hnau.base.data.possible.box.synchronized
import org.hnau.base.data.possible.getOrInitialize
import org.hnau.emitter.observing.push.always.AlwaysEmitter


class EmitterProperty<T>(
        private val source: MutableDelegate<T>
) : AlwaysEmitter<T>(), MutableDelegate<T> {

    private val cache = Box.synchronized<T>()

    override val value
        get() = cache.getOrInitialize { source.get() }

    private fun onChanged(newValue: T) {
        cache.set(newValue)
        onChanged()
        cache.clear()
    }

    override fun get() =
            source.get().also(::onChanged)

    override fun set(value: T) {
        source.set(value)
        onChanged(value)
    }

}

fun <T> MutableDelegate<T>.injectEmitter() =
        EmitterProperty(this)