package org.hnau.emitter.extensions

import org.hnau.base.data.single.Single
import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.me
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.lateinit.combineWith


fun <T> Emitter<T>.overlap(
        overlapSource: Emitter<Single<T>?>
) = combineWith(
        other = overlapSource
) { thisValue, overlapValue ->
    overlapValue.checkNullable(
            ifNotNull = Single<T>::value,
            ifNull = thisValue::me
    )
}