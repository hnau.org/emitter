package org.hnau.emitter.extensions

import org.hnau.emitter.Emitter


fun Emitter<String>.callIfEmpty() = callIf(String::isEmpty)
fun Emitter<String>.callIfNotEmpty() = callIf(String::isNotEmpty)

fun Emitter<String>.filterEmpty() = filter(String::isEmpty)
fun Emitter<String>.filterNotEmpty() = filter(String::isNotEmpty)
fun Emitter<String>.mapIsEmpty() = map(String::isEmpty).unique()
fun Emitter<String>.mapIsNotEmpty() = map(String::isNotEmpty).unique()