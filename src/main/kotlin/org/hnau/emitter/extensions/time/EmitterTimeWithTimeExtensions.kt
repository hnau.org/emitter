package org.hnau.emitter.extensions.time

import org.hnau.base.data.time.Time
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith

operator fun Emitter<Time>.plus(other: Emitter<Time>) = combineWith(other, Time::plus)
operator fun Emitter<Time>.minus(other: Emitter<Time>) = combineWith(other, Time::minus)
operator fun Emitter<Time>.div(other: Emitter<Time>) = combineWith(other, Time::div)
operator fun Emitter<Time>.rem(other: Emitter<Time>) = combineWith(other, Time::rem)
operator fun Emitter<Time>.rangeTo(other: Emitter<Time>) = combineWith(other) { thisValue, otherValue -> thisValue..otherValue }

operator fun Emitter<Time>.plus(other: Time) = map { it + other }
operator fun Emitter<Time>.minus(other: Time) = map { it - other }
operator fun Emitter<Time>.div(other: Time) = map { it / other }
operator fun Emitter<Time>.rem(other: Time) = map { it % other }
operator fun Emitter<Time>.rangeTo(other: Time) = map { it..other }

operator fun Time.plus(other: Emitter<Time>) = other.map { this + it }
operator fun Time.minus(other: Emitter<Time>) = other.map { this - it }
operator fun Time.div(other: Emitter<Time>) = other.map { this / it }
operator fun Time.rem(other: Emitter<Time>) = other.map { this % it }
operator fun Time.rangeTo(other: Emitter<Time>) = other.map { this..it }