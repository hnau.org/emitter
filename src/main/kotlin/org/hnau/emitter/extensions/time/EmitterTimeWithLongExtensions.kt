package org.hnau.emitter.extensions.time

import org.hnau.base.data.time.Time
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Time>.rem(other: Emitter<Long>) = combineWith(other, Time::rem)
operator fun Emitter<Time>.rem(other: Long) = map { it % other }
operator fun Time.rem(other: Emitter<Long>) = other.map { this % it }