package org.hnau.emitter.extensions.time

import org.hnau.base.data.time.Time
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Time>.rem(other: Emitter<Short>) = combineWith(other, Time::rem)
operator fun Emitter<Time>.rem(other: Short) = map { it % other }
operator fun Time.rem(other: Emitter<Short>) = other.map { this % it }