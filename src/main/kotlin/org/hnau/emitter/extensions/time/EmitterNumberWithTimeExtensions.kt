package org.hnau.emitter.extensions.time

import org.hnau.base.data.time.Time
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith

operator fun Emitter<Number>.times(other: Emitter<Time>) =
        combineWith(other) { number, time -> time * number }

operator fun Emitter<Number>.times(other: Time) = map { other * it }
operator fun Number.times(other: Emitter<Time>) = other.map { it * this }