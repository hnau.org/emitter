package org.hnau.emitter.extensions.time

import org.hnau.base.data.time.Time
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.mapIsEquals
import org.hnau.emitter.extensions.mapIsLargeThan
import org.hnau.emitter.extensions.mapIsLessThan
import org.hnau.emitter.extensions.mapIsNotEquals
import org.hnau.emitter.extensions.mapIsNotLargeThan
import org.hnau.emitter.extensions.mapIsNotLessThan


operator fun Emitter<Time>.unaryPlus() = this
operator fun Emitter<Time>.unaryMinus() = map(Time::unaryMinus)

fun Emitter<Time>.mapIsZero() = mapIsEquals(Time.zero)
fun Emitter<Time>.mapIsNotZero() = mapIsNotEquals(Time.zero)
fun Emitter<Time>.mapIsPositive() = mapIsLargeThan(Time.zero)
fun Emitter<Time>.mapIsNotPositive() = mapIsNotLargeThan(Time.zero)
fun Emitter<Time>.mapIsNegative() = mapIsLessThan(Time.zero)
fun Emitter<Time>.mapIsNotNegative() = mapIsNotLessThan(Time.zero)

fun Emitter<Time>.callIfZero() = callIf { it == Time.zero }
fun Emitter<Time>.callIfPositive() = callIf { it > Time.zero }
fun Emitter<Time>.callIfNegative() = callIf { it < Time.zero }
fun Emitter<Time>.callIfNotZero() = callIf { it != Time.zero }
fun Emitter<Time>.callIfNotPositive() = callIf { it <= Time.zero }
fun Emitter<Time>.callIfNotNegative() = callIf { it >= Time.zero }

fun Emitter<Time>.filterZero() = filter { it == Time.zero }
fun Emitter<Time>.filterPositive() = filter { it > Time.zero }
fun Emitter<Time>.filterNegative() = filter { it < Time.zero }
fun Emitter<Time>.filterNotZero() = filter { it != Time.zero }
fun Emitter<Time>.filterNotPositive() = filter { it <= Time.zero }
fun Emitter<Time>.filterNotNegative() = filter { it >= Time.zero }