package org.hnau.emitter.extensions.time

import org.hnau.base.data.time.Time
import org.hnau.base.data.time.fromMicroseconds
import org.hnau.base.data.time.fromNanoseconds
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


fun Emitter<Number>.asNanoseconds() =
        map { Time.fromNanoseconds(it) }

fun Emitter<Number>.asMicroseconds() =
        map { Time.fromMicroseconds(it) }

fun Emitter<Number>.asMilliseconds() =
        map { Time.millisecond * it }

fun Emitter<Number>.asSeconds() =
        map { Time.second * it }

fun Emitter<Number>.asMinutes() =
        map { Time.minute * it }

fun Emitter<Number>.asHours() =
        map { Time.hour * it }

fun Emitter<Number>.asDays() =
        map { Time.day * it }