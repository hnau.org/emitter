package org.hnau.emitter.extensions.time

import org.hnau.base.data.time.Time
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Time>.times(other: Emitter<Number>) = combineWith(other, Time::times)
operator fun Emitter<Time>.div(other: Emitter<Number>) = combineWith(other, Time::div)

operator fun Emitter<Time>.times(other: Number) = map { it * other }
operator fun Emitter<Time>.div(other: Number) = map { it / other }

operator fun Time.times(other: Emitter<Number>) = other.map { this * it }
operator fun Time.div(other: Emitter<Number>) = other.map { this / it }