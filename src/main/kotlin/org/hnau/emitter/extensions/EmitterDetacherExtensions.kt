package org.hnau.emitter.extensions

import org.hnau.emitter.Emitter
import org.hnau.emitter.EmitterDetacher
import org.hnau.emitter.utils.EmitterDetachers


inline fun <T> Emitter<T>.observeWithEmitterDetacher(
        crossinline observer: (value: T, detacher: EmitterDetacher) -> Unit
) {
    var detacher: EmitterDetacher? = null
    var detached = false
    val detacherInner: EmitterDetacher =  {
        detached = true
        detacher?.invoke()
    }

    detacher = observe { value -> observer(value, detacherInner) }

    if (detached) {
        detacher()
    }
}