package org.hnau.emitter.extensions

import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.always.line


inline fun <T, R> Iterable<Emitter<T>>.fold(
        initial: R,
        crossinline operation: (accumulator: R, item: T?) -> R
) = line {
    it.fold(initial, operation)
}

inline fun <T, R> Iterable<Emitter<T>>.foldIndexed(
        initial: R,
        crossinline operation: (index: Int, accumulator: R, item: T?) -> R
) = line {
    it.foldIndexed(initial, operation)
}

inline fun <T, R> Iterable<Emitter<T>>.foldRight(
        initial: R,
        crossinline operation: (item: T?, accumulator: R) -> R
) = line {
    it.foldRight(initial, operation)
}

inline fun <T, R> Iterable<Emitter<T>>.foldRightIndexed(
        initial: R,
        crossinline operation: (index: Int, item: T?, accumulator: R) -> R
) = line {
    it.foldRightIndexed(initial, operation)
}