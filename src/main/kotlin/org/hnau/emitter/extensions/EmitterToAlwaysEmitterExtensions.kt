package org.hnau.emitter.extensions

import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.synchronized
import org.hnau.base.extensions.it
import org.hnau.emitter.EmitterDetacher
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.always.AlwaysEmitter


fun <T> Emitter<T>.makeAlways(
        ifNoValue: () -> T
) = object : AlwaysEmitter<T>() {

    private val sourceValue = Lateinit.synchronized<T>()

    override val value: T
        get() = sourceValue.checkPossible(
                ifValueExists = ::it,
                ifValueNotExists = ifNoValue
        )

    private var detacher: EmitterDetacher? = null
        set(value) = synchronized(this) {
            field?.invoke()
            field = value
        }

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        detacher = this@makeAlways.observe { newSourceValue ->
            sourceValue.set(newSourceValue)
            onChanged()
        }
    }

    override fun afterLastDetached() {
        super.afterLastDetached()
        detacher = null
    }

}