package org.hnau.emitter.extensions.coerce

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combine


fun <T : Comparable<T>> Emitter<T>.coerceAtLeast(minimumValue: Emitter<T>) =
        Emitter.combine(this, minimumValue) { first, second -> first.coerceAtLeast(second) }

fun <T : Comparable<T>> Emitter<T>.coerceAtLeast(minimumValue: T) =
        map { it.coerceAtLeast(minimumValue) }

fun <T : Comparable<T>> T.coerceAtLeast(minimumValue: Emitter<T>) =
        minimumValue.map { this.coerceAtLeast(it) }