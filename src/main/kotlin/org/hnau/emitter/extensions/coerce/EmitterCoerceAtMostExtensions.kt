package org.hnau.emitter.extensions.coerce

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combine


fun <T : Comparable<T>> Emitter<T>.coerceAtMost(maximumValue: Emitter<T>) =
        Emitter.combine(this, maximumValue) { first, second -> first.coerceAtMost(second) }

fun <T : Comparable<T>> Emitter<T>.coerceAtMost(maximumValue: T) =
        map { it.coerceAtMost(maximumValue) }

fun <T : Comparable<T>> T.coerceAtMost(maximumValue: Emitter<T>) =
        maximumValue.map { this.coerceAtMost(it) }