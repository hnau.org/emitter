package org.hnau.emitter.extensions.coerce

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combine


fun <T : Comparable<T>> Emitter<T>.coerceIn(range: Emitter<ClosedFloatingPointRange<T>>) =
        Emitter.combine(this, range) { first, second -> first.coerceIn(second) }

fun <T : Comparable<T>> Emitter<T>.coerceIn(range: ClosedFloatingPointRange<T>) =
        map { it.coerceIn(range) }

fun <T : Comparable<T>> T.coerceIn(range: Emitter<ClosedFloatingPointRange<T>>) =
        range.map { this.coerceIn(it) }