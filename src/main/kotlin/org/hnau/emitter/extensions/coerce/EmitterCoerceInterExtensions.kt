package org.hnau.emitter.extensions.coerce

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combine
import org.hnau.emitter.observing.push.lateinit.combineWith


fun <T : Comparable<T>> Emitter<T>.coerceIn(minimumValue: Emitter<T>, maximumValue: Emitter<T>) =
        coerceAtLeast(minimumValue).coerceAtMost(maximumValue)

fun <T : Comparable<T>> Emitter<T>.coerceIn(minimumValue: T, maximumValue: Emitter<T>) =
        combineWith(maximumValue) { it, maximum -> it.coerceIn(minimumValue, maximum) }

fun <T : Comparable<T>> Emitter<T>.coerceIn(minimumValue: Emitter<T>, maximumValue: T) =
        combineWith(minimumValue) { it, minimum -> it.coerceIn(minimum, maximumValue) }

fun <T : Comparable<T>> T.coerceIn(minimumValue: Emitter<T>, maximumValue: Emitter<T>) =
        Emitter.combine(minimumValue, maximumValue) {minimum, maximum -> coerceIn(minimum, maximum)}

fun <T : Comparable<T>> T.coerceIn(minimumValue: T, maximumValue: Emitter<T>) =
        maximumValue.map { maximum -> coerceIn(minimumValue, maximum) }

fun <T : Comparable<T>> T.coerceIn(minimumValue: Emitter<T>, maximumValue: T) =
        minimumValue.map { minimum -> coerceIn(minimum, maximumValue) }

fun <T : Comparable<T>> Emitter<T>.coerceIn(minimumValue: T, maximumValue: T) =
        map { it.coerceIn(minimumValue, maximumValue) }