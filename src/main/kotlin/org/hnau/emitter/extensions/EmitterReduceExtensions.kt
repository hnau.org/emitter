package org.hnau.emitter.extensions

import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.always.line


inline fun <S, T : S> Iterable<Emitter<T>>.reduce(
        crossinline operation: (accumulator: S?, item: T?) -> S
) = line { it.reduce(operation) }

inline fun <S, T : S> Iterable<Emitter<T>>.reduceIndexed(
        crossinline operation: (index: Int, accumulator: S?, item: T?) -> S
) = line { it.reduceIndexed(operation) }

inline fun <S, T : S> Iterable<Emitter<T>>.reduceRight(
        crossinline operation: (item: T?, accumulator: S?) -> S
) = line { it.reduceRight(operation) }

inline fun <S, T : S> Iterable<Emitter<T>>.reduceRightIndexed(
        crossinline operation: (index: Int, item: T?, accumulator: S?) -> S
) = line { it.reduceRightIndexed(operation) }