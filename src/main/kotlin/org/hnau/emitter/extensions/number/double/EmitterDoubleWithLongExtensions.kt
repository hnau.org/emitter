package org.hnau.emitter.extensions.number.double

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Double>.plus(other: Emitter<Long>) = combineWith(other, Double::plus)
operator fun Emitter<Double>.minus(other: Emitter<Long>) = combineWith(other, Double::minus)
operator fun Emitter<Double>.times(other: Emitter<Long>) = combineWith(other, Double::times)
operator fun Emitter<Double>.div(other: Emitter<Long>) = combineWith(other, Double::div)
operator fun Emitter<Double>.rem(other: Emitter<Long>) = combineWith(other, Double::rem)

operator fun Emitter<Double>.plus(other: Long) = map { it + other }
operator fun Emitter<Double>.minus(other: Long) = map { it - other }
operator fun Emitter<Double>.times(other: Long) = map { it * other }
operator fun Emitter<Double>.div(other: Long) = map { it / other }
operator fun Emitter<Double>.rem(other: Long) = map { it % other }

operator fun Double.plus(other: Emitter<Long>) = other.map { this + it }
operator fun Double.minus(other: Emitter<Long>) = other.map { this - it }
operator fun Double.times(other: Emitter<Long>) = other.map { this * it }
operator fun Double.div(other: Emitter<Long>) = other.map { this / it }
operator fun Double.rem(other: Emitter<Long>) = other.map { this % it }