package org.hnau.emitter.extensions.number.long

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Long>.plus(other: Emitter<Double>) = combineWith(other, Long::plus)
operator fun Emitter<Long>.minus(other: Emitter<Double>) = combineWith(other, Long::minus)
operator fun Emitter<Long>.times(other: Emitter<Double>) = combineWith(other, Long::times)
operator fun Emitter<Long>.div(other: Emitter<Double>) = combineWith(other, Long::div)
operator fun Emitter<Long>.rem(other: Emitter<Double>) = combineWith(other, Long::rem)

operator fun Emitter<Long>.plus(other: Double) = map { it + other }
operator fun Emitter<Long>.minus(other: Double) = map { it - other }
operator fun Emitter<Long>.times(other: Double) = map { it * other }
operator fun Emitter<Long>.div(other: Double) = map { it / other }
operator fun Emitter<Long>.rem(other: Double) = map { it % other }

operator fun Long.plus(other: Emitter<Double>) = other.map { this + it }
operator fun Long.minus(other: Emitter<Double>) = other.map { this - it }
operator fun Long.times(other: Emitter<Double>) = other.map { this * it }
operator fun Long.div(other: Emitter<Double>) = other.map { this / it }
operator fun Long.rem(other: Emitter<Double>) = other.map { this % it }