package org.hnau.emitter.extensions.number.int

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique

fun Emitter<ShortArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Emitter<ShortArray>.callIfNotEmpty() = callIf { it.isNotEmpty() }

fun Emitter<ShortArray>.filterEmpty() = filter { it.isEmpty() }
fun Emitter<ShortArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Emitter<ShortArray>.mapIsEmpty() = map { it.isEmpty() }.unique()
fun Emitter<ShortArray>.mapIsNotEmpty() = map { it.isNotEmpty() }.unique()