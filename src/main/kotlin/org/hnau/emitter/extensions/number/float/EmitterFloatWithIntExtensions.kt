package org.hnau.emitter.extensions.number.float

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Float>.plus(other: Emitter<Int>) = combineWith(other, Float::plus)
operator fun Emitter<Float>.minus(other: Emitter<Int>) = combineWith(other, Float::minus)
operator fun Emitter<Float>.times(other: Emitter<Int>) = combineWith(other, Float::times)
operator fun Emitter<Float>.div(other: Emitter<Int>) = combineWith(other, Float::div)
operator fun Emitter<Float>.rem(other: Emitter<Int>) = combineWith(other, Float::rem)

operator fun Emitter<Float>.plus(other: Int) = map { it + other }
operator fun Emitter<Float>.minus(other: Int) = map { it - other }
operator fun Emitter<Float>.times(other: Int) = map { it * other }
operator fun Emitter<Float>.div(other: Int) = map { it / other }
operator fun Emitter<Float>.rem(other: Int) = map { it % other }

operator fun Float.plus(other: Emitter<Int>) = other.map { this + it }
operator fun Float.minus(other: Emitter<Int>) = other.map { this - it }
operator fun Float.times(other: Emitter<Int>) = other.map { this * it }
operator fun Float.div(other: Emitter<Int>) = other.map { this / it }
operator fun Float.rem(other: Emitter<Int>) = other.map { this % it }