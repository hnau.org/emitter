package org.hnau.emitter.extensions.number.byte

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Byte>.plus(other: Emitter<Short>) = combineWith(other, Byte::plus)
operator fun Emitter<Byte>.minus(other: Emitter<Short>) = combineWith(other, Byte::minus)
operator fun Emitter<Byte>.times(other: Emitter<Short>) = combineWith(other, Byte::times)
operator fun Emitter<Byte>.div(other: Emitter<Short>) = combineWith(other, Byte::div)
operator fun Emitter<Byte>.rem(other: Emitter<Short>) = combineWith(other, Byte::rem)
operator fun Emitter<Byte>.rangeTo(other: Emitter<Short>) = combineWith<Byte, Short, IntRange>(other, Byte::rangeTo)

operator fun Emitter<Byte>.plus(other: Short) = map { it + other }
operator fun Emitter<Byte>.minus(other: Short) = map { it - other }
operator fun Emitter<Byte>.times(other: Short) = map { it * other }
operator fun Emitter<Byte>.div(other: Short) = map { it / other }
operator fun Emitter<Byte>.rem(other: Short) = map { it % other }
operator fun Emitter<Byte>.rangeTo(other: Short) = map { it .. other }

operator fun Byte.plus(other: Emitter<Short>) = other.map { this + it }
operator fun Byte.minus(other: Emitter<Short>) = other.map { this - it }
operator fun Byte.times(other: Emitter<Short>) = other.map { this * it }
operator fun Byte.div(other: Emitter<Short>) = other.map { this / it }
operator fun Byte.rem(other: Emitter<Short>) = other.map { this % it }
operator fun Byte.rangeTo(other: Emitter<Short>) = other.map { this .. it }