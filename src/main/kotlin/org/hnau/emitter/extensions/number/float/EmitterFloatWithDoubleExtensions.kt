package org.hnau.emitter.extensions.number.float

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Float>.plus(other: Emitter<Double>) = combineWith(other, Float::plus)
operator fun Emitter<Float>.minus(other: Emitter<Double>) = combineWith(other, Float::minus)
operator fun Emitter<Float>.times(other: Emitter<Double>) = combineWith(other, Float::times)
operator fun Emitter<Float>.div(other: Emitter<Double>) = combineWith(other, Float::div)
operator fun Emitter<Float>.rem(other: Emitter<Double>) = combineWith(other, Float::rem)

operator fun Emitter<Float>.plus(other: Double) = map { it + other }
operator fun Emitter<Float>.minus(other: Double) = map { it - other }
operator fun Emitter<Float>.times(other: Double) = map { it * other }
operator fun Emitter<Float>.div(other: Double) = map { it / other }
operator fun Emitter<Float>.rem(other: Double) = map { it % other }

operator fun Float.plus(other: Emitter<Double>) = other.map { this + it }
operator fun Float.minus(other: Emitter<Double>) = other.map { this - it }
operator fun Float.times(other: Emitter<Double>) = other.map { this * it }
operator fun Float.div(other: Emitter<Double>) = other.map { this / it }
operator fun Float.rem(other: Emitter<Double>) = other.map { this % it }