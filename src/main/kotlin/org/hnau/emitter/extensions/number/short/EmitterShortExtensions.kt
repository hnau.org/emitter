package org.hnau.emitter.extensions.number.short

import org.hnau.base.extensions.boolean.toBoolean
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.observing.push.lateinit.combineWith
import kotlin.experimental.inv


operator fun Emitter<Short>.unaryPlus() = this
operator fun Emitter<Short>.unaryMinus() = map(Short::unaryMinus)
operator fun Emitter<Short>.inc() = map(Short::inc)
operator fun Emitter<Short>.dec() = map(Short::dec)

fun Emitter<Short>.inv() = map(Short::inv)

fun Emitter<Short>.callIfZero() = callIf { it.toInt() == 0 }
fun Emitter<Short>.callIfPositive() = callIf { it > 0 }
fun Emitter<Short>.callIfNegative() = callIf { it < 0 }
fun Emitter<Short>.callIfNotZero() = callIf { it.toInt() != 0 }
fun Emitter<Short>.callIfNotPositive() = callIf { it <= 0 }
fun Emitter<Short>.callIfNotNegative() = callIf { it >= 0 }

fun Emitter<Short>.filterZero() = filter { it.toInt() == 0 }
fun Emitter<Short>.filterPositive() = filter { it > 0 }
fun Emitter<Short>.filterNegative() = filter { it < 0 }
fun Emitter<Short>.filterNotZero() = filter { it.toInt() != 0 }
fun Emitter<Short>.filterNotPositive() = filter { it <= 0 }
fun Emitter<Short>.filterNotNegative() = filter { it >= 0 }

fun Emitter<Short>.mapIsZero() = map { it.toInt() == 0 }.unique()
fun Emitter<Short>.mapIsPositive() = map { it > 0 }.unique()
fun Emitter<Short>.mapIsNegative() = map { it < 0 }.unique()
fun Emitter<Short>.mapIsNotZero() = map { it.toInt() != 0 }.unique()
fun Emitter<Short>.mapIsNotPositive() = map { it <= 0 }.unique()
fun Emitter<Short>.mapIsNotNegative() = map { it >= 0 }.unique()

fun Emitter<Short>.toBoolean() = map(Short::toBoolean).unique()