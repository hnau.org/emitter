package org.hnau.emitter.extensions.number.short

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Short>.plus(other: Emitter<Byte>) = combineWith(other, Short::plus)
operator fun Emitter<Short>.minus(other: Emitter<Byte>) = combineWith(other, Short::minus)
operator fun Emitter<Short>.times(other: Emitter<Byte>) = combineWith(other, Short::times)
operator fun Emitter<Short>.div(other: Emitter<Byte>) = combineWith(other, Short::div)
operator fun Emitter<Short>.rem(other: Emitter<Byte>) = combineWith(other, Short::rem)
operator fun Emitter<Short>.rangeTo(other: Emitter<Byte>) = combineWith<Short, Byte, IntRange>(other, Short::rangeTo)

operator fun Emitter<Short>.plus(other: Byte) = map { it + other }
operator fun Emitter<Short>.minus(other: Byte) = map { it - other }
operator fun Emitter<Short>.times(other: Byte) = map { it * other }
operator fun Emitter<Short>.div(other: Byte) = map { it / other }
operator fun Emitter<Short>.rem(other: Byte) = map { it % other }
operator fun Emitter<Short>.rangeTo(other: Byte) = map { it .. other }

operator fun Short.plus(other: Emitter<Byte>) = other.map { this + it }
operator fun Short.minus(other: Emitter<Byte>) = other.map { this - it }
operator fun Short.times(other: Emitter<Byte>) = other.map { this * it }
operator fun Short.div(other: Emitter<Byte>) = other.map { this / it }
operator fun Short.rem(other: Emitter<Byte>) = other.map { this % it }
operator fun Short.rangeTo(other: Emitter<Byte>) = other.map { this .. it }