package org.hnau.emitter.extensions.number.float

import org.hnau.base.extensions.boolean.toBoolean
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Float>.rangeTo(other: Emitter<Float>) = combineWith(other) { a, b -> a..b }
operator fun Emitter<Float>.rangeTo(other: Float) = map { it..other }
operator fun Float.rangeTo(other: Emitter<Float>) = other.map { this..it }

operator fun Emitter<Float>.unaryPlus() = this
operator fun Emitter<Float>.unaryMinus() = map(Float::unaryMinus)
operator fun Emitter<Float>.inc() = map(Float::inc)
operator fun Emitter<Float>.dec() = map(Float::dec)

fun Emitter<Float>.callIfZero() = callIf { it == 0f }
fun Emitter<Float>.callIfPositive() = callIf { it > 0 }
fun Emitter<Float>.callIfNegative() = callIf { it < 0 }
fun Emitter<Float>.callIfNotZero() = callIf { it != 0f }
fun Emitter<Float>.callIfNotPositive() = callIf { it <= 0 }
fun Emitter<Float>.callIfNotNegative() = callIf { it >= 0 }

fun Emitter<Float>.filterZero() = filter { it == 0f }
fun Emitter<Float>.filterPositive() = filter { it > 0 }
fun Emitter<Float>.filterNegative() = filter { it < 0 }
fun Emitter<Float>.filterNotZero() = filter { it != 0f }
fun Emitter<Float>.filterNotPositive() = filter { it <= 0 }
fun Emitter<Float>.filterNotNegative() = filter { it >= 0 }

fun Emitter<Float>.mapIsZero() = map { it == 0f }.unique()
fun Emitter<Float>.mapIsPositive() = map { it > 0 }.unique()
fun Emitter<Float>.mapIsNegative() = map { it < 0 }.unique()
fun Emitter<Float>.mapIsNotZero() = map { it != 0f }.unique()
fun Emitter<Float>.mapIsNotPositive() = map { it <= 0 }.unique()
fun Emitter<Float>.mapIsNotNegative() = map { it >= 0 }.unique()

fun Emitter<Float>.toBoolean() = map(Float::toBoolean).unique()