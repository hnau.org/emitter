package org.hnau.emitter.extensions.number.int

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Int>.plus(other: Emitter<Short>) = combineWith(other, Int::plus)
operator fun Emitter<Int>.minus(other: Emitter<Short>) = combineWith(other, Int::minus)
operator fun Emitter<Int>.times(other: Emitter<Short>) = combineWith(other, Int::times)
operator fun Emitter<Int>.div(other: Emitter<Short>) = combineWith(other, Int::div)
operator fun Emitter<Int>.rem(other: Emitter<Short>) = combineWith(other, Int::rem)
operator fun Emitter<Int>.rangeTo(other: Emitter<Short>) = combineWith<Int, Short, IntRange>(other, Int::rangeTo)

operator fun Emitter<Int>.plus(other: Short) = map { it + other }
operator fun Emitter<Int>.minus(other: Short) = map { it - other }
operator fun Emitter<Int>.times(other: Short) = map { it * other }
operator fun Emitter<Int>.div(other: Short) = map { it / other }
operator fun Emitter<Int>.rem(other: Short) = map { it % other }
operator fun Emitter<Int>.rangeTo(other: Short) = map { it .. other }

operator fun Int.plus(other: Emitter<Short>) = other.map { this + it }
operator fun Int.minus(other: Emitter<Short>) = other.map { this - it }
operator fun Int.times(other: Emitter<Short>) = other.map { this * it }
operator fun Int.div(other: Emitter<Short>) = other.map { this / it }
operator fun Int.rem(other: Emitter<Short>) = other.map { this % it }
operator fun Int.rangeTo(other: Emitter<Short>) = other.map { this .. it }