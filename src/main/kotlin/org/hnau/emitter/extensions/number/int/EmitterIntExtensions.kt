package org.hnau.emitter.extensions.number.int

import org.hnau.base.extensions.boolean.toBoolean
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Int>.unaryPlus() = this
operator fun Emitter<Int>.unaryMinus() = map(Int::unaryMinus)
operator fun Emitter<Int>.inc() = map(Int::inc)
operator fun Emitter<Int>.dec() = map(Int::dec)

fun Emitter<Int>.inv() = map(Int::inv)

infix fun Emitter<Int>.ushr(bitsCount: Emitter<Int>) = combineWith(bitsCount, Int::ushr)
infix fun Emitter<Int>.shr(bitsCount: Emitter<Int>) = combineWith(bitsCount, Int::shr)
infix fun Emitter<Int>.shl(bitsCount: Emitter<Int>) = combineWith(bitsCount, Int::shl)
infix fun Emitter<Int>.ushr(bitsCount: Int) = map { it ushr bitsCount }
infix fun Emitter<Int>.shr(bitsCount: Int) = map { it shr bitsCount }
infix fun Emitter<Int>.shl(bitsCount: Int) = map { it shl bitsCount }
infix fun Int.ushr(bitsCount: Emitter<Int>) = bitsCount.map { this ushr it }
infix fun Int.shr(bitsCount: Emitter<Int>) =  bitsCount.map { this shr it }
infix fun Int.shl(bitsCount: Emitter<Int>) =  bitsCount.map { this shl it }

fun Emitter<Int>.callIfZero() = callIf { it == 0 }
fun Emitter<Int>.callIfPositive() = callIf { it > 0 }
fun Emitter<Int>.callIfNegative() = callIf { it < 0 }
fun Emitter<Int>.callIfNotZero() = callIf { it != 0 }
fun Emitter<Int>.callIfNotPositive() = callIf { it <= 0 }
fun Emitter<Int>.callIfNotNegative() = callIf { it >= 0 }

fun Emitter<Int>.filterZero() = filter { it == 0 }
fun Emitter<Int>.filterPositive() = filter { it > 0 }
fun Emitter<Int>.filterNegative() = filter { it < 0 }
fun Emitter<Int>.filterNotZero() = filter { it != 0 }
fun Emitter<Int>.filterNotPositive() = filter { it <= 0 }
fun Emitter<Int>.filterNotNegative() = filter { it >= 0 }

fun Emitter<Int>.mapIsZero() = map { it == 0 }.unique()
fun Emitter<Int>.mapIsPositive() = map { it > 0 }.unique()
fun Emitter<Int>.mapIsNegative() = map { it < 0 }.unique()
fun Emitter<Int>.mapIsNotZero() = map { it != 0 }.unique()
fun Emitter<Int>.mapIsNotPositive() = map { it <= 0 }.unique()
fun Emitter<Int>.mapIsNotNegative() = map { it >= 0 }.unique()

fun Emitter<Int>.toBoolean() = map(Int::toBoolean).unique()