package org.hnau.emitter.extensions.number.byte

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Byte>.plus(other: Emitter<Float>) = combineWith(other, Byte::plus)
operator fun Emitter<Byte>.minus(other: Emitter<Float>) = combineWith(other, Byte::minus)
operator fun Emitter<Byte>.times(other: Emitter<Float>) = combineWith(other, Byte::times)
operator fun Emitter<Byte>.div(other: Emitter<Float>) = combineWith(other, Byte::div)
operator fun Emitter<Byte>.rem(other: Emitter<Float>) = combineWith(other, Byte::rem)

operator fun Emitter<Byte>.plus(other: Float) = map { it + other }
operator fun Emitter<Byte>.minus(other: Float) = map { it - other }
operator fun Emitter<Byte>.times(other: Float) = map { it * other }
operator fun Emitter<Byte>.div(other: Float) = map { it / other }
operator fun Emitter<Byte>.rem(other: Float) = map { it % other }

operator fun Byte.plus(other: Emitter<Float>) = other.map { this + it }
operator fun Byte.minus(other: Emitter<Float>) = other.map { this - it }
operator fun Byte.times(other: Emitter<Float>) = other.map { this * it }
operator fun Byte.div(other: Emitter<Float>) = other.map { this / it }
operator fun Byte.rem(other: Emitter<Float>) = other.map { this % it }