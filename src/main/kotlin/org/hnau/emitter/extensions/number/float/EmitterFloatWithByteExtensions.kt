package org.hnau.emitter.extensions.number.float

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Float>.plus(other: Emitter<Byte>) = combineWith(other, Float::plus)
operator fun Emitter<Float>.minus(other: Emitter<Byte>) = combineWith(other, Float::minus)
operator fun Emitter<Float>.times(other: Emitter<Byte>) = combineWith(other, Float::times)
operator fun Emitter<Float>.div(other: Emitter<Byte>) = combineWith(other, Float::div)
operator fun Emitter<Float>.rem(other: Emitter<Byte>) = combineWith(other, Float::rem)

operator fun Emitter<Float>.plus(other: Byte) = map { it + other }
operator fun Emitter<Float>.minus(other: Byte) = map { it - other }
operator fun Emitter<Float>.times(other: Byte) = map { it * other }
operator fun Emitter<Float>.div(other: Byte) = map { it / other }
operator fun Emitter<Float>.rem(other: Byte) = map { it % other }

operator fun Float.plus(other: Emitter<Byte>) = other.map { this + it }
operator fun Float.minus(other: Emitter<Byte>) = other.map { this - it }
operator fun Float.times(other: Emitter<Byte>) = other.map { this * it }
operator fun Float.div(other: Emitter<Byte>) = other.map { this / it }
operator fun Float.rem(other: Emitter<Byte>) = other.map { this % it }