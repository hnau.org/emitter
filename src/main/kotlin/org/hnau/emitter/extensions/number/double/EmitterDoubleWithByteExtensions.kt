package org.hnau.emitter.extensions.number.double

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Double>.plus(other: Emitter<Byte>) = combineWith(other, Double::plus)
operator fun Emitter<Double>.minus(other: Emitter<Byte>) = combineWith(other, Double::minus)
operator fun Emitter<Double>.times(other: Emitter<Byte>) = combineWith(other, Double::times)
operator fun Emitter<Double>.div(other: Emitter<Byte>) = combineWith(other, Double::div)
operator fun Emitter<Double>.rem(other: Emitter<Byte>) = combineWith(other, Double::rem)

operator fun Emitter<Double>.plus(other: Byte) = map { it + other }
operator fun Emitter<Double>.minus(other: Byte) = map { it - other }
operator fun Emitter<Double>.times(other: Byte) = map { it * other }
operator fun Emitter<Double>.div(other: Byte) = map { it / other }
operator fun Emitter<Double>.rem(other: Byte) = map { it % other }

operator fun Double.plus(other: Emitter<Byte>) = other.map { this + it }
operator fun Double.minus(other: Emitter<Byte>) = other.map { this - it }
operator fun Double.times(other: Emitter<Byte>) = other.map { this * it }
operator fun Double.div(other: Emitter<Byte>) = other.map { this / it }
operator fun Double.rem(other: Emitter<Byte>) = other.map { this % it }