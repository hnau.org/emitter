package org.hnau.emitter.extensions.number.int

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Int>.plus(other: Emitter<Byte>) = combineWith(other, Int::plus)
operator fun Emitter<Int>.minus(other: Emitter<Byte>) = combineWith(other, Int::minus)
operator fun Emitter<Int>.times(other: Emitter<Byte>) = combineWith(other, Int::times)
operator fun Emitter<Int>.div(other: Emitter<Byte>) = combineWith(other, Int::div)
operator fun Emitter<Int>.rem(other: Emitter<Byte>) = combineWith(other, Int::rem)
operator fun Emitter<Int>.rangeTo(other: Emitter<Byte>) = combineWith<Int, Byte, IntRange>(other, Int::rangeTo)

operator fun Emitter<Int>.plus(other: Byte) = map { it + other }
operator fun Emitter<Int>.minus(other: Byte) = map { it - other }
operator fun Emitter<Int>.times(other: Byte) = map { it * other }
operator fun Emitter<Int>.div(other: Byte) = map { it / other }
operator fun Emitter<Int>.rem(other: Byte) = map { it % other }
operator fun Emitter<Int>.rangeTo(other: Byte) = map { it..other }

operator fun Int.plus(other: Emitter<Byte>) = other.map { this + it }
operator fun Int.minus(other: Emitter<Byte>) = other.map { this - it }
operator fun Int.times(other: Emitter<Byte>) = other.map { this * it }
operator fun Int.div(other: Emitter<Byte>) = other.map { this / it }
operator fun Int.rem(other: Emitter<Byte>) = other.map { this % it }
operator fun Int.rangeTo(other: Emitter<Byte>) = other.map { this..it }