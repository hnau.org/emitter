package org.hnau.emitter.extensions.number.int

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Int>.plus(other: Emitter<Int>) = combineWith(other, Int::plus)
operator fun Emitter<Int>.minus(other: Emitter<Int>) = combineWith(other, Int::minus)
operator fun Emitter<Int>.times(other: Emitter<Int>) = combineWith(other, Int::times)
operator fun Emitter<Int>.div(other: Emitter<Int>) = combineWith(other, Int::div)
operator fun Emitter<Int>.rem(other: Emitter<Int>) = combineWith(other, Int::rem)
operator fun Emitter<Int>.rangeTo(other: Emitter<Int>) = combineWith(other, Int::rangeTo)

operator fun Emitter<Int>.plus(other: Int) = map { it + other }
operator fun Emitter<Int>.minus(other: Int) = map { it - other }
operator fun Emitter<Int>.times(other: Int) = map { it * other }
operator fun Emitter<Int>.div(other: Int) = map { it / other }
operator fun Emitter<Int>.rem(other: Int) = map { it % other }
operator fun Emitter<Int>.rangeTo(other: Int) = map { it .. other }

operator fun Int.plus(other: Emitter<Int>) = other.map { this + it }
operator fun Int.minus(other: Emitter<Int>) = other.map { this - it }
operator fun Int.times(other: Emitter<Int>) = other.map { this * it }
operator fun Int.div(other: Emitter<Int>) = other.map { this / it }
operator fun Int.rem(other: Emitter<Int>) = other.map { this % it }
operator fun Int.rangeTo(other: Emitter<Int>) = other.map { this .. it }

infix fun Emitter<Int>.and(other: Emitter<Int>) = combineWith(other, Int::and)
infix fun Emitter<Int>.or(other: Emitter<Int>) = combineWith(other, Int::or)
infix fun Emitter<Int>.xor(other: Emitter<Int>) = combineWith(other, Int::xor)
infix fun Emitter<Int>.and(other: Int) = map { it and other }
infix fun Emitter<Int>.or(other: Int) = map { it or other }
infix fun Emitter<Int>.xor(other: Int) = map { it xor other }
infix fun Int.and(other: Emitter<Int>) = other.map { this and it }
infix fun Int.or(other: Emitter<Int>) = other.map { this or it }
infix fun Int.xor(other: Emitter<Int>) = other.map { this xor it }