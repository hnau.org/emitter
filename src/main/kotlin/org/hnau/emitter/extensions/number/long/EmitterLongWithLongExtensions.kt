package org.hnau.emitter.extensions.number.long

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Long>.plus(other: Emitter<Long>) = combineWith(other, Long::plus)
operator fun Emitter<Long>.minus(other: Emitter<Long>) = combineWith(other, Long::minus)
operator fun Emitter<Long>.times(other: Emitter<Long>) = combineWith(other, Long::times)
operator fun Emitter<Long>.div(other: Emitter<Long>) = combineWith(other, Long::div)
operator fun Emitter<Long>.rem(other: Emitter<Long>) = combineWith(other, Long::rem)
operator fun Emitter<Long>.rangeTo(other: Emitter<Long>) = combineWith<Long, Long, LongRange>(other, Long::rangeTo)

operator fun Emitter<Long>.plus(other: Long) = map { it + other }
operator fun Emitter<Long>.minus(other: Long) = map { it - other }
operator fun Emitter<Long>.times(other: Long) = map { it * other }
operator fun Emitter<Long>.div(other: Long) = map { it / other }
operator fun Emitter<Long>.rem(other: Long) = map { it % other }
operator fun Emitter<Long>.rangeTo(other: Long) = map { it..other }

operator fun Long.plus(other: Emitter<Long>) = other.map { this + it }
operator fun Long.minus(other: Emitter<Long>) = other.map { this - it }
operator fun Long.times(other: Emitter<Long>) = other.map { this * it }
operator fun Long.div(other: Emitter<Long>) = other.map { this / it }
operator fun Long.rem(other: Emitter<Long>) = other.map { this % it }
operator fun Long.rangeTo(other: Emitter<Long>) = other.map { this..it }

infix fun Emitter<Long>.and(other: Emitter<Long>) = combineWith(other, Long::and)
infix fun Emitter<Long>.or(other: Emitter<Long>) = combineWith(other, Long::or)
infix fun Emitter<Long>.xor(other: Emitter<Long>) = combineWith(other, Long::xor)
infix fun Emitter<Long>.and(other: Long) = map { it and other }
infix fun Emitter<Long>.or(other: Long) = map { it or other }
infix fun Emitter<Long>.xor(other: Long) = map { it xor other }
infix fun Long.and(other: Emitter<Long>) = other.map { this and it }
infix fun Long.or(other: Emitter<Long>) = other.map { this or it }
infix fun Long.xor(other: Emitter<Long>) = other.map { this xor it }