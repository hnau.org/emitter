package org.hnau.emitter.extensions.number.byte

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Byte>.plus(other: Emitter<Byte>) = combineWith(other, Byte::plus)
operator fun Emitter<Byte>.minus(other: Emitter<Byte>) = combineWith(other, Byte::minus)
operator fun Emitter<Byte>.times(other: Emitter<Byte>) = combineWith(other, Byte::times)
operator fun Emitter<Byte>.div(other: Emitter<Byte>) = combineWith(other, Byte::div)
operator fun Emitter<Byte>.rem(other: Emitter<Byte>) = combineWith(other, Byte::rem)
operator fun Emitter<Byte>.rangeTo(other: Emitter<Byte>) = combineWith<Byte, Byte, IntRange>(other, Byte::rangeTo)

operator fun Emitter<Byte>.plus(other: Byte) = map { it + other }
operator fun Emitter<Byte>.minus(other: Byte) = map { it - other }
operator fun Emitter<Byte>.times(other: Byte) = map { it * other }
operator fun Emitter<Byte>.div(other: Byte) = map { it / other }
operator fun Emitter<Byte>.rem(other: Byte) = map { it % other }
operator fun Emitter<Byte>.rangeTo(other: Byte) = map { it..other }

operator fun Byte.plus(other: Emitter<Byte>) = other.map { this + it }
operator fun Byte.minus(other: Emitter<Byte>) = other.map { this - it }
operator fun Byte.times(other: Emitter<Byte>) = other.map { this * it }
operator fun Byte.div(other: Emitter<Byte>) = other.map { this / it }
operator fun Byte.rem(other: Emitter<Byte>) = other.map { this % it }
operator fun Byte.rangeTo(other: Emitter<Byte>) = other.map { this..it }