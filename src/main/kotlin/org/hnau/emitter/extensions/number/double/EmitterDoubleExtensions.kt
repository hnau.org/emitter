package org.hnau.emitter.extensions.number.double

import org.hnau.base.extensions.boolean.toBoolean
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Double>.rangeTo(other: Emitter<Double>) = combineWith(other) { a, b -> a..b }
operator fun Emitter<Double>.rangeTo(other: Double) = map { it..other }
operator fun Double.rangeTo(other: Emitter<Double>) = other.map { this..it }

operator fun Emitter<Double>.unaryPlus() = this
operator fun Emitter<Double>.unaryMinus() = map(Double::unaryMinus)
operator fun Emitter<Double>.inc() = map(Double::inc)
operator fun Emitter<Double>.dec() = map(Double::dec)

fun Emitter<Double>.callIfZero() = callIf { it == 0.0 }
fun Emitter<Double>.callIfPositive() = callIf { it > 0 }
fun Emitter<Double>.callIfNegative() = callIf { it < 0 }
fun Emitter<Double>.callIfNotZero() = callIf { it != 0.0 }
fun Emitter<Double>.callIfNotPositive() = callIf { it <= 0 }
fun Emitter<Double>.callIfNotNegative() = callIf { it >= 0 }

fun Emitter<Double>.filterZero() = filter { it == 0.0 }
fun Emitter<Double>.filterPositive() = filter { it > 0 }
fun Emitter<Double>.filterNegative() = filter { it < 0 }
fun Emitter<Double>.filterNotZero() = filter { it != 0.0 }
fun Emitter<Double>.filterNotPositive() = filter { it <= 0 }
fun Emitter<Double>.filterNotNegative() = filter { it >= 0 }

fun Emitter<Double>.mapIsZero() = map { it == 0.0 }.unique()
fun Emitter<Double>.mapIsPositive() = map { it > 0 }.unique()
fun Emitter<Double>.mapIsNegative() = map { it < 0 }.unique()
fun Emitter<Double>.mapIsNotZero() = map { it != 0.0 }.unique()
fun Emitter<Double>.mapIsNotPositive() = map { it <= 0 }.unique()
fun Emitter<Double>.mapIsNotNegative() = map { it >= 0 }.unique()

fun Emitter<Double>.toBoolean() = map(Double::toBoolean).unique()