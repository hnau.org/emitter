package org.hnau.emitter.extensions.number.short

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Short>.plus(other: Emitter<Int>) = combineWith(other, Short::plus)
operator fun Emitter<Short>.minus(other: Emitter<Int>) = combineWith(other, Short::minus)
operator fun Emitter<Short>.times(other: Emitter<Int>) = combineWith(other, Short::times)
operator fun Emitter<Short>.div(other: Emitter<Int>) = combineWith(other, Short::div)
operator fun Emitter<Short>.rem(other: Emitter<Int>) = combineWith(other, Short::rem)
operator fun Emitter<Short>.rangeTo(other: Emitter<Int>) = combineWith<Short, Int, IntRange>(other, Short::rangeTo)

operator fun Emitter<Short>.plus(other: Int) = map { it + other }
operator fun Emitter<Short>.minus(other: Int) = map { it - other }
operator fun Emitter<Short>.times(other: Int) = map { it * other }
operator fun Emitter<Short>.div(other: Int) = map { it / other }
operator fun Emitter<Short>.rem(other: Int) = map { it % other }
operator fun Emitter<Short>.rangeTo(other: Int) = map { it .. other }

operator fun Short.plus(other: Emitter<Int>) = other.map { this + it }
operator fun Short.minus(other: Emitter<Int>) = other.map { this - it }
operator fun Short.times(other: Emitter<Int>) = other.map { this * it }
operator fun Short.div(other: Emitter<Int>) = other.map { this / it }
operator fun Short.rem(other: Emitter<Int>) = other.map { this % it }
operator fun Short.rangeTo(other: Emitter<Int>) = other.map { this .. it }