package org.hnau.emitter.extensions.number.int

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique

fun Emitter<IntArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Emitter<IntArray>.callIfNotEmpty() = callIf { it.isNotEmpty() }

fun Emitter<IntArray>.filterEmpty() = filter { it.isEmpty() }
fun Emitter<IntArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Emitter<IntArray>.mapIsEmpty() = map { it.isEmpty() }.unique()
fun Emitter<IntArray>.mapIsNotEmpty() = map { it.isNotEmpty() }.unique()