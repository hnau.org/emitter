package org.hnau.emitter.extensions.number.float

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Float>.plus(other: Emitter<Short>) = combineWith(other, Float::plus)
operator fun Emitter<Float>.minus(other: Emitter<Short>) = combineWith(other, Float::minus)
operator fun Emitter<Float>.times(other: Emitter<Short>) = combineWith(other, Float::times)
operator fun Emitter<Float>.div(other: Emitter<Short>) = combineWith(other, Float::div)
operator fun Emitter<Float>.rem(other: Emitter<Short>) = combineWith(other, Float::rem)

operator fun Emitter<Float>.plus(other: Short) = map { it + other }
operator fun Emitter<Float>.minus(other: Short) = map { it - other }
operator fun Emitter<Float>.times(other: Short) = map { it * other }
operator fun Emitter<Float>.div(other: Short) = map { it / other }
operator fun Emitter<Float>.rem(other: Short) = map { it % other }

operator fun Float.plus(other: Emitter<Short>) = other.map { this + it }
operator fun Float.minus(other: Emitter<Short>) = other.map { this - it }
operator fun Float.times(other: Emitter<Short>) = other.map { this * it }
operator fun Float.div(other: Emitter<Short>) = other.map { this / it }
operator fun Float.rem(other: Emitter<Short>) = other.map { this % it }