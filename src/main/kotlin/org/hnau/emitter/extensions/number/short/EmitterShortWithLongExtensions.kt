package org.hnau.emitter.extensions.number.short

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Short>.plus(other: Emitter<Long>) = combineWith(other, Short::plus)
operator fun Emitter<Short>.minus(other: Emitter<Long>) = combineWith(other, Short::minus)
operator fun Emitter<Short>.times(other: Emitter<Long>) = combineWith(other, Short::times)
operator fun Emitter<Short>.div(other: Emitter<Long>) = combineWith(other, Short::div)
operator fun Emitter<Short>.rem(other: Emitter<Long>) = combineWith(other, Short::rem)
operator fun Emitter<Short>.rangeTo(other: Emitter<Long>) = combineWith<Short, Long, LongRange>(other, Short::rangeTo)

operator fun Emitter<Short>.plus(other: Long) = map { it + other }
operator fun Emitter<Short>.minus(other: Long) = map { it - other }
operator fun Emitter<Short>.times(other: Long) = map { it * other }
operator fun Emitter<Short>.div(other: Long) = map { it / other }
operator fun Emitter<Short>.rem(other: Long) = map { it % other }
operator fun Emitter<Short>.rangeTo(other: Long) = map { it .. other }

operator fun Short.plus(other: Emitter<Long>) = other.map { this + it }
operator fun Short.minus(other: Emitter<Long>) = other.map { this - it }
operator fun Short.times(other: Emitter<Long>) = other.map { this * it }
operator fun Short.div(other: Emitter<Long>) = other.map { this / it }
operator fun Short.rem(other: Emitter<Long>) = other.map { this % it }
operator fun Short.rangeTo(other: Emitter<Long>) = other.map { this .. it }