package org.hnau.emitter.extensions.number.double

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique

fun Emitter<DoubleArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Emitter<DoubleArray>.callIfNotEmpty() = callIf { it.isNotEmpty() }

fun Emitter<DoubleArray>.filterEmpty() = filter { it.isEmpty() }
fun Emitter<DoubleArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Emitter<DoubleArray>.mapIsEmpty() = map { it.isEmpty() }.unique()
fun Emitter<DoubleArray>.mapIsNotEmpty() = map { it.isNotEmpty() }.unique()