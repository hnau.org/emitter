package org.hnau.emitter.extensions.number.long

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Long>.plus(other: Emitter<Float>) = combineWith(other, Long::plus)
operator fun Emitter<Long>.minus(other: Emitter<Float>) = combineWith(other, Long::minus)
operator fun Emitter<Long>.times(other: Emitter<Float>) = combineWith(other, Long::times)
operator fun Emitter<Long>.div(other: Emitter<Float>) = combineWith(other, Long::div)
operator fun Emitter<Long>.rem(other: Emitter<Float>) = combineWith(other, Long::rem)

operator fun Emitter<Long>.plus(other: Float) = map { it + other }
operator fun Emitter<Long>.minus(other: Float) = map { it - other }
operator fun Emitter<Long>.times(other: Float) = map { it * other }
operator fun Emitter<Long>.div(other: Float) = map { it / other }
operator fun Emitter<Long>.rem(other: Float) = map { it % other }

operator fun Long.plus(other: Emitter<Float>) = other.map { this + it }
operator fun Long.minus(other: Emitter<Float>) = other.map { this - it }
operator fun Long.times(other: Emitter<Float>) = other.map { this * it }
operator fun Long.div(other: Emitter<Float>) = other.map { this / it }
operator fun Long.rem(other: Emitter<Float>) = other.map { this % it }