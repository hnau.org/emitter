package org.hnau.emitter.extensions.number.long

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique

fun Emitter<LongArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Emitter<LongArray>.callIfNotEmpty() = callIf { it.isNotEmpty() }

fun Emitter<LongArray>.filterEmpty() = filter { it.isEmpty() }
fun Emitter<LongArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Emitter<LongArray>.mapIsEmpty() = map { it.isEmpty() }.unique()
fun Emitter<LongArray>.mapIsNotEmpty() = map { it.isNotEmpty() }.unique()