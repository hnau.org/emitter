package org.hnau.emitter.extensions.number.float

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Float>.plus(other: Emitter<Float>) = combineWith(other, Float::plus)
operator fun Emitter<Float>.minus(other: Emitter<Float>) = combineWith(other, Float::minus)
operator fun Emitter<Float>.times(other: Emitter<Float>) = combineWith(other, Float::times)
operator fun Emitter<Float>.div(other: Emitter<Float>) = combineWith(other, Float::div)
operator fun Emitter<Float>.rem(other: Emitter<Float>) = combineWith(other, Float::rem)

operator fun Emitter<Float>.plus(other: Float) = map { it + other }
operator fun Emitter<Float>.minus(other: Float) = map { it - other }
operator fun Emitter<Float>.times(other: Float) = map { it * other }
operator fun Emitter<Float>.div(other: Float) = map { it / other }
operator fun Emitter<Float>.rem(other: Float) = map { it % other }

operator fun Float.plus(other: Emitter<Float>) = other.map { this + it }
operator fun Float.minus(other: Emitter<Float>) = other.map { this - it }
operator fun Float.times(other: Emitter<Float>) = other.map { this * it }
operator fun Float.div(other: Emitter<Float>) = other.map { this / it }
operator fun Float.rem(other: Emitter<Float>) = other.map { this % it }