package org.hnau.emitter.extensions.number.short

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Short>.plus(other: Emitter<Float>) = combineWith(other, Short::plus)
operator fun Emitter<Short>.minus(other: Emitter<Float>) = combineWith(other, Short::minus)
operator fun Emitter<Short>.times(other: Emitter<Float>) = combineWith(other, Short::times)
operator fun Emitter<Short>.div(other: Emitter<Float>) = combineWith(other, Short::div)
operator fun Emitter<Short>.rem(other: Emitter<Float>) = combineWith(other, Short::rem)

operator fun Emitter<Short>.plus(other: Float) = map { it + other }
operator fun Emitter<Short>.minus(other: Float) = map { it - other }
operator fun Emitter<Short>.times(other: Float) = map { it * other }
operator fun Emitter<Short>.div(other: Float) = map { it / other }
operator fun Emitter<Short>.rem(other: Float) = map { it % other }

operator fun Short.plus(other: Emitter<Float>) = other.map { this + it }
operator fun Short.minus(other: Emitter<Float>) = other.map { this - it }
operator fun Short.times(other: Emitter<Float>) = other.map { this * it }
operator fun Short.div(other: Emitter<Float>) = other.map { this / it }
operator fun Short.rem(other: Emitter<Float>) = other.map { this % it }