package org.hnau.emitter.extensions.number.int

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Int>.plus(other: Emitter<Double>) = combineWith(other, Int::plus)
operator fun Emitter<Int>.minus(other: Emitter<Double>) = combineWith(other, Int::minus)
operator fun Emitter<Int>.times(other: Emitter<Double>) = combineWith(other, Int::times)
operator fun Emitter<Int>.div(other: Emitter<Double>) = combineWith(other, Int::div)
operator fun Emitter<Int>.rem(other: Emitter<Double>) = combineWith(other, Int::rem)

operator fun Emitter<Int>.plus(other: Double) = map { it + other }
operator fun Emitter<Int>.minus(other: Double) = map { it - other }
operator fun Emitter<Int>.times(other: Double) = map { it * other }
operator fun Emitter<Int>.div(other: Double) = map { it / other }
operator fun Emitter<Int>.rem(other: Double) = map { it % other }

operator fun Int.plus(other: Emitter<Double>) = other.map { this + it }
operator fun Int.minus(other: Emitter<Double>) = other.map { this - it }
operator fun Int.times(other: Emitter<Double>) = other.map { this * it }
operator fun Int.div(other: Emitter<Double>) = other.map { this / it }
operator fun Int.rem(other: Emitter<Double>) = other.map { this % it }