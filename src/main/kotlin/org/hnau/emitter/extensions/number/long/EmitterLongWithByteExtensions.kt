package org.hnau.emitter.extensions.number.long

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Long>.plus(other: Emitter<Byte>) = combineWith(other, Long::plus)
operator fun Emitter<Long>.minus(other: Emitter<Byte>) = combineWith(other, Long::minus)
operator fun Emitter<Long>.times(other: Emitter<Byte>) = combineWith(other, Long::times)
operator fun Emitter<Long>.div(other: Emitter<Byte>) = combineWith(other, Long::div)
operator fun Emitter<Long>.rem(other: Emitter<Byte>) = combineWith(other, Long::rem)
operator fun Emitter<Long>.rangeTo(other: Emitter<Byte>) = combineWith<Long, Byte, LongRange>(other, Long::rangeTo)

operator fun Emitter<Long>.plus(other: Byte) = map { it + other }
operator fun Emitter<Long>.minus(other: Byte) = map { it - other }
operator fun Emitter<Long>.times(other: Byte) = map { it * other }
operator fun Emitter<Long>.div(other: Byte) = map { it / other }
operator fun Emitter<Long>.rem(other: Byte) = map { it % other }
operator fun Emitter<Long>.rangeTo(other: Byte) = map { it .. other }

operator fun Long.plus(other: Emitter<Byte>) = other.map { this + it }
operator fun Long.minus(other: Emitter<Byte>) = other.map { this - it }
operator fun Long.times(other: Emitter<Byte>) = other.map { this * it }
operator fun Long.div(other: Emitter<Byte>) = other.map { this / it }
operator fun Long.rem(other: Emitter<Byte>) = other.map { this % it }
operator fun Long.rangeTo(other: Emitter<Byte>) = other.map { this .. it }