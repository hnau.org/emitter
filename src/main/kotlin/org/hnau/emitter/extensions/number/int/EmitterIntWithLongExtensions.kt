package org.hnau.emitter.extensions.number.int

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Int>.plus(other: Emitter<Long>) = combineWith(other, Int::plus)
operator fun Emitter<Int>.minus(other: Emitter<Long>) = combineWith(other, Int::minus)
operator fun Emitter<Int>.times(other: Emitter<Long>) = combineWith(other, Int::times)
operator fun Emitter<Int>.div(other: Emitter<Long>) = combineWith(other, Int::div)
operator fun Emitter<Int>.rem(other: Emitter<Long>) = combineWith(other, Int::rem)
operator fun Emitter<Int>.rangeTo(other: Emitter<Long>) = combineWith<Int, Long, LongRange>(other, Int::rangeTo)

operator fun Emitter<Int>.plus(other: Long) = map { it + other }
operator fun Emitter<Int>.minus(other: Long) = map { it - other }
operator fun Emitter<Int>.times(other: Long) = map { it * other }
operator fun Emitter<Int>.div(other: Long) = map { it / other }
operator fun Emitter<Int>.rem(other: Long) = map { it % other }
operator fun Emitter<Int>.rangeTo(other: Long) = map { it .. other }

operator fun Int.plus(other: Emitter<Long>) = other.map { this + it }
operator fun Int.minus(other: Emitter<Long>) = other.map { this - it }
operator fun Int.times(other: Emitter<Long>) = other.map { this * it }
operator fun Int.div(other: Emitter<Long>) = other.map { this / it }
operator fun Int.rem(other: Emitter<Long>) = other.map { this % it }
operator fun Int.rangeTo(other: Emitter<Long>) = other.map { this .. it }