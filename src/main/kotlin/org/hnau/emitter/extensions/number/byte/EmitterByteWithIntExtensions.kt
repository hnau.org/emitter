package org.hnau.emitter.extensions.number.byte

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Byte>.plus(other: Emitter<Int>) = combineWith(other, Byte::plus)
operator fun Emitter<Byte>.minus(other: Emitter<Int>) = combineWith(other, Byte::minus)
operator fun Emitter<Byte>.times(other: Emitter<Int>) = combineWith(other, Byte::times)
operator fun Emitter<Byte>.div(other: Emitter<Int>) = combineWith(other, Byte::div)
operator fun Emitter<Byte>.rem(other: Emitter<Int>) = combineWith(other, Byte::rem)
operator fun Emitter<Byte>.rangeTo(other: Emitter<Int>) = combineWith(other, Byte::rangeTo)

operator fun Emitter<Byte>.plus(other: Int) = map { it + other }
operator fun Emitter<Byte>.minus(other: Int) = map { it - other }
operator fun Emitter<Byte>.times(other: Int) = map { it * other }
operator fun Emitter<Byte>.div(other: Int) = map { it / other }
operator fun Emitter<Byte>.rem(other: Int) = map { it % other }
operator fun Emitter<Byte>.rangeTo(other: Int) = map { it .. other }

operator fun Byte.plus(other: Emitter<Int>) = other.map { this + it }
operator fun Byte.minus(other: Emitter<Int>) = other.map { this - it }
operator fun Byte.times(other: Emitter<Int>) = other.map { this * it }
operator fun Byte.div(other: Emitter<Int>) = other.map { this / it }
operator fun Byte.rem(other: Emitter<Int>) = other.map { this % it }
operator fun Byte.rangeTo(other: Emitter<Int>) = other.map { this .. it }