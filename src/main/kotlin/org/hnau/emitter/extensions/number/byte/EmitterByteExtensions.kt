package org.hnau.emitter.extensions.number.byte

import org.hnau.base.extensions.boolean.toBoolean
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.observing.push.lateinit.combineWith
import kotlin.experimental.inv


operator fun Emitter<Byte>.unaryPlus() = this
operator fun Emitter<Byte>.unaryMinus() = map(Byte::unaryMinus)
operator fun Emitter<Byte>.inc() = map(Byte::inc)
operator fun Emitter<Byte>.dec() = map(Byte::dec)

fun Emitter<Byte>.inv() = map(Byte::inv)

fun Emitter<Byte>.callIfZero() = callIf { it.toInt() == 0 }
fun Emitter<Byte>.callIfPositive() = callIf { it > 0 }
fun Emitter<Byte>.callIfNegative() = callIf { it < 0 }
fun Emitter<Byte>.callIfNotZero() = callIf { it.toInt() != 0 }
fun Emitter<Byte>.callIfNotPositive() = callIf { it <= 0 }
fun Emitter<Byte>.callIfNotNegative() = callIf { it >= 0 }

fun Emitter<Byte>.filterZero() = filter { it.toInt() == 0 }
fun Emitter<Byte>.filterPositive() = filter { it > 0 }
fun Emitter<Byte>.filterNegative() = filter { it < 0 }
fun Emitter<Byte>.filterNotZero() = filter { it.toInt() != 0 }
fun Emitter<Byte>.filterNotPositive() = filter { it <= 0 }
fun Emitter<Byte>.filterNotNegative() = filter { it >= 0 }

fun Emitter<Byte>.mapIsZero() = map { it.toInt() == 0 }.unique()
fun Emitter<Byte>.mapIsPositive() = map { it > 0 }.unique()
fun Emitter<Byte>.mapIsNegative() = map { it < 0 }.unique()
fun Emitter<Byte>.mapIsNotZero() = map { it.toInt() != 0 }.unique()
fun Emitter<Byte>.mapIsNotPositive() = map { it <= 0 }.unique()
fun Emitter<Byte>.mapIsNotNegative() = map { it >= 0 }.unique()

fun Emitter<Byte>.toBoolean() = map(Byte::toBoolean).unique()