package org.hnau.emitter.extensions.number.int

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Int>.plus(other: Emitter<Float>) = combineWith(other, Int::plus)
operator fun Emitter<Int>.minus(other: Emitter<Float>) = combineWith(other, Int::minus)
operator fun Emitter<Int>.times(other: Emitter<Float>) = combineWith(other, Int::times)
operator fun Emitter<Int>.div(other: Emitter<Float>) = combineWith(other, Int::div)
operator fun Emitter<Int>.rem(other: Emitter<Float>) = combineWith(other, Int::rem)

operator fun Emitter<Int>.plus(other: Float) = map { it + other }
operator fun Emitter<Int>.minus(other: Float) = map { it - other }
operator fun Emitter<Int>.times(other: Float) = map { it * other }
operator fun Emitter<Int>.div(other: Float) = map { it / other }
operator fun Emitter<Int>.rem(other: Float) = map { it % other }

operator fun Int.plus(other: Emitter<Float>) = other.map { this + it }
operator fun Int.minus(other: Emitter<Float>) = other.map { this - it }
operator fun Int.times(other: Emitter<Float>) = other.map { this * it }
operator fun Int.div(other: Emitter<Float>) = other.map { this / it }
operator fun Int.rem(other: Emitter<Float>) = other.map { this % it }