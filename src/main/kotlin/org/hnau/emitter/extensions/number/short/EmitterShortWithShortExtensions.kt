package org.hnau.emitter.extensions.number.short

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Short>.plus(other: Emitter<Short>) = combineWith(other, Short::plus)
operator fun Emitter<Short>.minus(other: Emitter<Short>) = combineWith(other, Short::minus)
operator fun Emitter<Short>.times(other: Emitter<Short>) = combineWith(other, Short::times)
operator fun Emitter<Short>.div(other: Emitter<Short>) = combineWith(other, Short::div)
operator fun Emitter<Short>.rem(other: Emitter<Short>) = combineWith(other, Short::rem)
operator fun Emitter<Short>.rangeTo(other: Emitter<Short>) = combineWith<Short, Short, IntRange>(other, Short::rangeTo)

operator fun Emitter<Short>.plus(other: Short) = map { it + other }
operator fun Emitter<Short>.minus(other: Short) = map { it - other }
operator fun Emitter<Short>.times(other: Short) = map { it * other }
operator fun Emitter<Short>.div(other: Short) = map { it / other }
operator fun Emitter<Short>.rem(other: Short) = map { it % other }
operator fun Emitter<Short>.rangeTo(other: Short) = map { it..other }

operator fun Short.plus(other: Emitter<Short>) = other.map { this + it }
operator fun Short.minus(other: Emitter<Short>) = other.map { this - it }
operator fun Short.times(other: Emitter<Short>) = other.map { this * it }
operator fun Short.div(other: Emitter<Short>) = other.map { this / it }
operator fun Short.rem(other: Emitter<Short>) = other.map { this % it }
operator fun Short.rangeTo(other: Emitter<Short>) = other.map { this..it }