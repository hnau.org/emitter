package org.hnau.emitter.extensions.number.double

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Double>.plus(other: Emitter<Double>) = combineWith(other, Double::plus)
operator fun Emitter<Double>.minus(other: Emitter<Double>) = combineWith(other, Double::minus)
operator fun Emitter<Double>.times(other: Emitter<Double>) = combineWith(other, Double::times)
operator fun Emitter<Double>.div(other: Emitter<Double>) = combineWith(other, Double::div)
operator fun Emitter<Double>.rem(other: Emitter<Double>) = combineWith(other, Double::rem)

operator fun Emitter<Double>.plus(other: Double) = map { it + other }
operator fun Emitter<Double>.minus(other: Double) = map { it - other }
operator fun Emitter<Double>.times(other: Double) = map { it * other }
operator fun Emitter<Double>.div(other: Double) = map { it / other }
operator fun Emitter<Double>.rem(other: Double) = map { it % other }

operator fun Double.plus(other: Emitter<Double>) = other.map { this + it }
operator fun Double.minus(other: Emitter<Double>) = other.map { this - it }
operator fun Double.times(other: Emitter<Double>) = other.map { this * it }
operator fun Double.div(other: Emitter<Double>) = other.map { this / it }
operator fun Double.rem(other: Emitter<Double>) = other.map { this % it }