package org.hnau.emitter.extensions.number.double

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Double>.plus(other: Emitter<Int>) = combineWith(other, Double::plus)
operator fun Emitter<Double>.minus(other: Emitter<Int>) = combineWith(other, Double::minus)
operator fun Emitter<Double>.times(other: Emitter<Int>) = combineWith(other, Double::times)
operator fun Emitter<Double>.div(other: Emitter<Int>) = combineWith(other, Double::div)
operator fun Emitter<Double>.rem(other: Emitter<Int>) = combineWith(other, Double::rem)

operator fun Emitter<Double>.plus(other: Int) = map { it + other }
operator fun Emitter<Double>.minus(other: Int) = map { it - other }
operator fun Emitter<Double>.times(other: Int) = map { it * other }
operator fun Emitter<Double>.div(other: Int) = map { it / other }
operator fun Emitter<Double>.rem(other: Int) = map { it % other }

operator fun Double.plus(other: Emitter<Int>) = other.map { this + it }
operator fun Double.minus(other: Emitter<Int>) = other.map { this - it }
operator fun Double.times(other: Emitter<Int>) = other.map { this * it }
operator fun Double.div(other: Emitter<Int>) = other.map { this / it }
operator fun Double.rem(other: Emitter<Int>) = other.map { this % it }