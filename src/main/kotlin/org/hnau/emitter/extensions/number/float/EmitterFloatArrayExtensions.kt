package org.hnau.emitter.extensions.number.float

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique

fun Emitter<FloatArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Emitter<FloatArray>.callIfNotEmpty() = callIf { it.isNotEmpty() }

fun Emitter<FloatArray>.filterEmpty() = filter { it.isEmpty() }
fun Emitter<FloatArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Emitter<FloatArray>.mapIsEmpty() = map { it.isEmpty() }.unique()
fun Emitter<FloatArray>.mapIsNotEmpty() = map { it.isNotEmpty() }.unique()