package org.hnau.emitter.extensions.number.byte

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique

fun Emitter<ByteArray>.callIfEmpty() = callIf { it.isEmpty() }
fun Emitter<ByteArray>.callIfNotEmpty() = callIf { it.isNotEmpty() }

fun Emitter<ByteArray>.filterEmpty() = filter { it.isEmpty() }
fun Emitter<ByteArray>.filterNotEmpty() = filter { it.isNotEmpty() }
fun Emitter<ByteArray>.mapIsEmpty() = map { it.isEmpty() }.unique()
fun Emitter<ByteArray>.mapIsNotEmpty() = map { it.isNotEmpty() }.unique()