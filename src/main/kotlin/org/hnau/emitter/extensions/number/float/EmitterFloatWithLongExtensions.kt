package org.hnau.emitter.extensions.number.float

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Float>.plus(other: Emitter<Long>) = combineWith(other, Float::plus)
operator fun Emitter<Float>.minus(other: Emitter<Long>) = combineWith(other, Float::minus)
operator fun Emitter<Float>.times(other: Emitter<Long>) = combineWith(other, Float::times)
operator fun Emitter<Float>.div(other: Emitter<Long>) = combineWith(other, Float::div)
operator fun Emitter<Float>.rem(other: Emitter<Long>) = combineWith(other, Float::rem)

operator fun Emitter<Float>.plus(other: Long) = map { it + other }
operator fun Emitter<Float>.minus(other: Long) = map { it - other }
operator fun Emitter<Float>.times(other: Long) = map { it * other }
operator fun Emitter<Float>.div(other: Long) = map { it / other }
operator fun Emitter<Float>.rem(other: Long) = map { it % other }

operator fun Float.plus(other: Emitter<Long>) = other.map { this + it }
operator fun Float.minus(other: Emitter<Long>) = other.map { this - it }
operator fun Float.times(other: Emitter<Long>) = other.map { this * it }
operator fun Float.div(other: Emitter<Long>) = other.map { this / it }
operator fun Float.rem(other: Emitter<Long>) = other.map { this % it }