package org.hnau.emitter.extensions.number

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map


fun Emitter<Number>.toByte() = map(Number::toByte)
fun Emitter<Number>.toShort() = map(Number::toShort)
fun Emitter<Number>.toInt() = map(Number::toInt)
fun Emitter<Number>.toLong() = map(Number::toLong)
fun Emitter<Number>.toFloat() = map(Number::toFloat)
fun Emitter<Number>.toDouble() = map(Number::toDouble)