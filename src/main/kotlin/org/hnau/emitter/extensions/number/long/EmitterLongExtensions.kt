package org.hnau.emitter.extensions.number.long

import org.hnau.base.extensions.boolean.toBoolean
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.callIf
import org.hnau.emitter.extensions.filter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.extensions.unique
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Long>.unaryPlus() = this
operator fun Emitter<Long>.unaryMinus() = map(Long::unaryMinus)
operator fun Emitter<Long>.inc() = map(Long::inc)
operator fun Emitter<Long>.dec() = map(Long::dec)

fun Emitter<Long>.inv() = map(Long::inv)

infix fun Emitter<Long>.ushr(bitsCount: Emitter<Int>) = combineWith(bitsCount, Long::ushr)
infix fun Emitter<Long>.shr(bitsCount: Emitter<Int>) = combineWith(bitsCount, Long::shr)
infix fun Emitter<Long>.shl(bitsCount: Emitter<Int>) = combineWith(bitsCount, Long::shl)
infix fun Emitter<Long>.ushr(bitsCount: Int) = map { it ushr bitsCount }
infix fun Emitter<Long>.shr(bitsCount: Int) = map { it shr bitsCount }
infix fun Emitter<Long>.shl(bitsCount: Int) = map { it shl bitsCount }
infix fun Long.ushr(bitsCount: Emitter<Int>) = bitsCount.map { this ushr it }
infix fun Long.shr(bitsCount: Emitter<Int>) = bitsCount.map { this shr it }
infix fun Long.shl(bitsCount: Emitter<Int>) = bitsCount.map { this shl it }

fun Emitter<Long>.callIfZero() = callIf { it == 0L }
fun Emitter<Long>.callIfPositive() = callIf { it > 0 }
fun Emitter<Long>.callIfNegative() = callIf { it < 0 }
fun Emitter<Long>.callIfNotZero() = callIf { it != 0L }
fun Emitter<Long>.callIfNotPositive() = callIf { it <= 0 }
fun Emitter<Long>.callIfNotNegative() = callIf { it >= 0 }

fun Emitter<Long>.filterZero() = filter { it == 0L }
fun Emitter<Long>.filterPositive() = filter { it > 0 }
fun Emitter<Long>.filterNegative() = filter { it < 0 }
fun Emitter<Long>.filterNotZero() = filter { it != 0L }
fun Emitter<Long>.filterNotPositive() = filter { it <= 0 }
fun Emitter<Long>.filterNotNegative() = filter { it >= 0 }

fun Emitter<Long>.mapIsZero() = map { it == 0L }.unique()
fun Emitter<Long>.mapIsPositive() = map { it > 0 }.unique()
fun Emitter<Long>.mapIsNegative() = map { it < 0 }.unique()
fun Emitter<Long>.mapIsNotZero() = map { it != 0L }.unique()
fun Emitter<Long>.mapIsNotPositive() = map { it <= 0 }.unique()
fun Emitter<Long>.mapIsNotNegative() = map { it >= 0 }.unique()

fun Emitter<Long>.toBoolean() = map(Long::toBoolean).unique()