package org.hnau.emitter.extensions.number.byte

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Byte>.plus(other: Emitter<Long>) = combineWith(other, Byte::plus)
operator fun Emitter<Byte>.minus(other: Emitter<Long>) = combineWith(other, Byte::minus)
operator fun Emitter<Byte>.times(other: Emitter<Long>) = combineWith(other, Byte::times)
operator fun Emitter<Byte>.div(other: Emitter<Long>) = combineWith(other, Byte::div)
operator fun Emitter<Byte>.rem(other: Emitter<Long>) = combineWith(other, Byte::rem)
operator fun Emitter<Byte>.rangeTo(other: Emitter<Long>) = combineWith<Byte, Long, LongRange>(other, Byte::rangeTo)

operator fun Emitter<Byte>.plus(other: Long) = map { it + other }
operator fun Emitter<Byte>.minus(other: Long) = map { it - other }
operator fun Emitter<Byte>.times(other: Long) = map { it * other }
operator fun Emitter<Byte>.div(other: Long) = map { it / other }
operator fun Emitter<Byte>.rem(other: Long) = map { it % other }
operator fun Emitter<Byte>.rangeTo(other: Long) = map { it .. other }

operator fun Byte.plus(other: Emitter<Long>) = other.map { this + it }
operator fun Byte.minus(other: Emitter<Long>) = other.map { this - it }
operator fun Byte.times(other: Emitter<Long>) = other.map { this * it }
operator fun Byte.div(other: Emitter<Long>) = other.map { this / it }
operator fun Byte.rem(other: Emitter<Long>) = other.map { this % it }
operator fun Byte.rangeTo(other: Emitter<Long>) = other.map { this .. it }