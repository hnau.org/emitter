package org.hnau.emitter.extensions.number.double

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Double>.plus(other: Emitter<Short>) = combineWith(other, Double::plus)
operator fun Emitter<Double>.minus(other: Emitter<Short>) = combineWith(other, Double::minus)
operator fun Emitter<Double>.times(other: Emitter<Short>) = combineWith(other, Double::times)
operator fun Emitter<Double>.div(other: Emitter<Short>) = combineWith(other, Double::div)
operator fun Emitter<Double>.rem(other: Emitter<Short>) = combineWith(other, Double::rem)

operator fun Emitter<Double>.plus(other: Short) = map { it + other }
operator fun Emitter<Double>.minus(other: Short) = map { it - other }
operator fun Emitter<Double>.times(other: Short) = map { it * other }
operator fun Emitter<Double>.div(other: Short) = map { it / other }
operator fun Emitter<Double>.rem(other: Short) = map { it % other }

operator fun Double.plus(other: Emitter<Short>) = other.map { this + it }
operator fun Double.minus(other: Emitter<Short>) = other.map { this - it }
operator fun Double.times(other: Emitter<Short>) = other.map { this * it }
operator fun Double.div(other: Emitter<Short>) = other.map { this / it }
operator fun Double.rem(other: Emitter<Short>) = other.map { this % it }