package org.hnau.emitter.extensions.number.long

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith



operator fun Emitter<Long>.plus(other: Emitter<Int>) = combineWith(other, Long::plus)
operator fun Emitter<Long>.minus(other: Emitter<Int>) = combineWith(other, Long::minus)
operator fun Emitter<Long>.times(other: Emitter<Int>) = combineWith(other, Long::times)
operator fun Emitter<Long>.div(other: Emitter<Int>) = combineWith(other, Long::div)
operator fun Emitter<Long>.rem(other: Emitter<Int>) = combineWith(other, Long::rem)
operator fun Emitter<Long>.rangeTo(other: Emitter<Int>) = combineWith<Long, Int, LongRange>(other, Long::rangeTo)

operator fun Emitter<Long>.plus(other: Int) = map { it + other }
operator fun Emitter<Long>.minus(other: Int) = map { it - other }
operator fun Emitter<Long>.times(other: Int) = map { it * other }
operator fun Emitter<Long>.div(other: Int) = map { it / other }
operator fun Emitter<Long>.rem(other: Int) = map { it % other }
operator fun Emitter<Long>.rangeTo(other: Int) = map { it .. other }

operator fun Long.plus(other: Emitter<Int>) = other.map { this + it }
operator fun Long.minus(other: Emitter<Int>) = other.map { this - it }
operator fun Long.times(other: Emitter<Int>) = other.map { this * it }
operator fun Long.div(other: Emitter<Int>) = other.map { this / it }
operator fun Long.rem(other: Emitter<Int>) = other.map { this % it }
operator fun Long.rangeTo(other: Emitter<Int>) = other.map { this .. it }