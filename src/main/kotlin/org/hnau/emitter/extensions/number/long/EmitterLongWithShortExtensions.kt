package org.hnau.emitter.extensions.number.long

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Long>.plus(other: Emitter<Short>) = combineWith(other, Long::plus)
operator fun Emitter<Long>.minus(other: Emitter<Short>) = combineWith(other, Long::minus)
operator fun Emitter<Long>.times(other: Emitter<Short>) = combineWith(other, Long::times)
operator fun Emitter<Long>.div(other: Emitter<Short>) = combineWith(other, Long::div)
operator fun Emitter<Long>.rem(other: Emitter<Short>) = combineWith(other, Long::rem)
operator fun Emitter<Long>.rangeTo(other: Emitter<Short>) = combineWith<Long, Short, LongRange>(other, Long::rangeTo)

operator fun Emitter<Long>.plus(other: Short) = map { it + other }
operator fun Emitter<Long>.minus(other: Short) = map { it - other }
operator fun Emitter<Long>.times(other: Short) = map { it * other }
operator fun Emitter<Long>.div(other: Short) = map { it / other }
operator fun Emitter<Long>.rem(other: Short) = map { it % other }
operator fun Emitter<Long>.rangeTo(other: Short) = map { it .. other }

operator fun Long.plus(other: Emitter<Short>) = other.map { this + it }
operator fun Long.minus(other: Emitter<Short>) = other.map { this - it }
operator fun Long.times(other: Emitter<Short>) = other.map { this * it }
operator fun Long.div(other: Emitter<Short>) = other.map { this / it }
operator fun Long.rem(other: Emitter<Short>) = other.map { this % it }
operator fun Long.rangeTo(other: Emitter<Short>) = other.map { this .. it }