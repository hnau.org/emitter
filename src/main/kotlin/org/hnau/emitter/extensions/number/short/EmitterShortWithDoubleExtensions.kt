package org.hnau.emitter.extensions.number.short

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Short>.plus(other: Emitter<Double>) = combineWith(other, Short::plus)
operator fun Emitter<Short>.minus(other: Emitter<Double>) = combineWith(other, Short::minus)
operator fun Emitter<Short>.times(other: Emitter<Double>) = combineWith(other, Short::times)
operator fun Emitter<Short>.div(other: Emitter<Double>) = combineWith(other, Short::div)
operator fun Emitter<Short>.rem(other: Emitter<Double>) = combineWith(other, Short::rem)

operator fun Emitter<Short>.plus(other: Double) = map { it + other }
operator fun Emitter<Short>.minus(other: Double) = map { it - other }
operator fun Emitter<Short>.times(other: Double) = map { it * other }
operator fun Emitter<Short>.div(other: Double) = map { it / other }
operator fun Emitter<Short>.rem(other: Double) = map { it % other }

operator fun Short.plus(other: Emitter<Double>) = other.map { this + it }
operator fun Short.minus(other: Emitter<Double>) = other.map { this - it }
operator fun Short.times(other: Emitter<Double>) = other.map { this * it }
operator fun Short.div(other: Emitter<Double>) = other.map { this / it }
operator fun Short.rem(other: Emitter<Double>) = other.map { this % it }