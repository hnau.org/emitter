package org.hnau.emitter.extensions.number.byte

import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.observing.push.lateinit.combineWith


operator fun Emitter<Byte>.plus(other: Emitter<Double>) = combineWith(other, Byte::plus)
operator fun Emitter<Byte>.minus(other: Emitter<Double>) = combineWith(other, Byte::minus)
operator fun Emitter<Byte>.times(other: Emitter<Double>) = combineWith(other, Byte::times)
operator fun Emitter<Byte>.div(other: Emitter<Double>) = combineWith(other, Byte::div)
operator fun Emitter<Byte>.rem(other: Emitter<Double>) = combineWith(other, Byte::rem)

operator fun Emitter<Byte>.plus(other: Double) = map { it + other }
operator fun Emitter<Byte>.minus(other: Double) = map { it - other }
operator fun Emitter<Byte>.times(other: Double) = map { it * other }
operator fun Emitter<Byte>.div(other: Double) = map { it / other }
operator fun Emitter<Byte>.rem(other: Double) = map { it % other }

operator fun Byte.plus(other: Emitter<Double>) = other.map { this + it }
operator fun Byte.minus(other: Emitter<Double>) = other.map { this - it }
operator fun Byte.times(other: Emitter<Double>) = other.map { this * it }
operator fun Byte.div(other: Emitter<Double>) = other.map { this / it }
operator fun Byte.rem(other: Emitter<Double>) = other.map { this % it }