package org.hnau.emitter.extensions

import org.hnau.emitter.Emitter
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


@Deprecated("Are you sure, that this method is indispensable for you? Apparently your code logic is not ideomatic, if you need this method")
suspend inline fun <T> Emitter<T>.wait() =
        suspendCoroutine<T> { continuation ->
            var finished = false
            observeWithEmitterDetacher { value, detacher ->
                value
                        .takeIf { !finished }
                        ?.let { valueInner ->
                            finished = true
                            detacher()
                            continuation.resume(valueInner)
                        }
            }

        }