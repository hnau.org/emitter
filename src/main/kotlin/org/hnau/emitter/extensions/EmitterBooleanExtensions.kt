package org.hnau.emitter.extensions

import org.hnau.base.extensions.boolean.toByte
import org.hnau.base.extensions.boolean.toDouble
import org.hnau.base.extensions.boolean.toFloat
import org.hnau.base.extensions.boolean.toInt
import org.hnau.base.extensions.boolean.toLong
import org.hnau.base.extensions.boolean.toShort
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.lateinit.combineWith


fun Emitter<Boolean>.callIfTrue() = callIf { it }
fun Emitter<Boolean>.callIfFalse() = callIf { !it }

operator fun Emitter<Boolean>.not() = map(Boolean::not)

infix fun Emitter<Boolean>.and(other: Emitter<Boolean>) = combineWith(other, Boolean::and).unique()
infix fun Emitter<Boolean>.or(other: Emitter<Boolean>) = combineWith(other, Boolean::or).unique()
infix fun Emitter<Boolean>.xor(other: Emitter<Boolean>) = combineWith(other, Boolean::xor).unique()

infix fun Emitter<Boolean>.and(other: Boolean) = map(other::and).unique()
infix fun Emitter<Boolean>.or(other: Boolean) = map(other::or).unique()
infix fun Emitter<Boolean>.xor(other: Boolean) = map(other::xor).unique()

infix fun Boolean.and(other: Emitter<Boolean>) = other.map(this::and).unique()
infix fun Boolean.or(other: Emitter<Boolean>) = other.map(this::or).unique()
infix fun Boolean.xor(other: Emitter<Boolean>) = other.map(this::xor).unique()

fun Emitter<Boolean>.toInt() = map(Boolean::toInt)
fun Emitter<Boolean>.toLong() = map(Boolean::toLong)
fun Emitter<Boolean>.toByte() = map(Boolean::toByte)
fun Emitter<Boolean>.toShort() = map(Boolean::toShort)
fun Emitter<Boolean>.toFloat() = map(Boolean::toFloat)
fun Emitter<Boolean>.toDouble() = map(Boolean::toDouble)
