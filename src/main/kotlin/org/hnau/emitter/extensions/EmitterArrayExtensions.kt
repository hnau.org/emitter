package org.hnau.emitter.extensions

import org.hnau.emitter.Emitter


fun <T> Emitter<Array<T>>.callIfEmpty() = callIf(Array<T>::isEmpty)
fun <T> Emitter<Array<T>>.callIfNotEmpty() = callIf(Array<T>::isNotEmpty)

fun <T> Emitter<Array<T>>.filterEmpty() = filter(Array<T>::isEmpty)
fun <T> Emitter<Array<T>>.filterNotEmpty() = filter(Array<T>::isNotEmpty)
fun <T> Emitter<Array<T>>.mapIsEmpty() = map(Array<T>::isEmpty).unique()
fun <T> Emitter<Array<T>>.mapIsNotEmpty() = map(Array<T>::isNotEmpty).unique()