package org.hnau.emitter.extensions

import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.lateinit.combine
import org.hnau.emitter.observing.push.lateinit.combineWith


fun <T, O> Emitter<T>.mapIsEquals(other: Emitter<O>) =
        combineWith(other) { thisValue, otherValue -> thisValue == otherValue }.unique()

fun <T, O> Emitter<T>.mapIsEquals(other: O) =
        map { it == other }.unique()

fun <T, O> T.mapIsEquals(other: Emitter<O>) =
        other.map { this == it }.unique()

fun <T, O> Emitter<T>.mapIsEqualsByReference(other: Emitter<O>) =
        combineWith(other) { thisValue, otherValue -> thisValue === otherValue }.unique()

fun <T, O> Emitter<T>.mapIsEqualsByReference(other: O) =
        map { it === other }.unique()

fun <T, O> T.mapIsEqualsByReference(other: Emitter<O>) =
        other.map { this === it }.unique()

fun <T, O> Emitter<T>.mapIsNotEquals(other: Emitter<O>) =
        combineWith(other) { thisValue, otherValue -> thisValue != otherValue }.unique()

fun <T, O> Emitter<T>.mapIsNotEquals(other: O) =
        map { it != other }.unique()

fun <T, O> T.mapIsNotEquals(other: Emitter<O>) =
        other.map { this != it }.unique()

fun <T, O> Emitter<T>.mapIsNotEqualsByReference(other: Emitter<O>) =
        combineWith(other) { thisValue, otherValue -> thisValue !== otherValue }.unique()

fun <T, O> Emitter<T>.mapIsNotEqualsByReference(other: O) =
        map { it !== other }.unique()

fun <T, O> T.mapIsNotEqualsByReference(other: Emitter<O>) =
        other.map { this !== it }.unique()

fun <O, T : Comparable<O>> Emitter<T>.mapIsLargeThan(other: Emitter<O>) =
        combineWith(other) { thisValue, otherValue -> thisValue > otherValue }.unique()

fun <O, T : Comparable<O>> Emitter<T>.mapIsLargeThan(other: O) =
        map { it > other }.unique()

fun <O, T : Comparable<O>> T.mapIsLargeThan(other: Emitter<O>) =
        other.map { this > it }.unique()


fun <O, T : Comparable<O>> Emitter<T>.mapIsLessThan(other: Emitter<O>) =
        combineWith(other) { thisValue, otherValue -> thisValue < otherValue }.unique()

fun <O, T : Comparable<O>> Emitter<T>.mapIsLessThan(other: O) =
        map { it < other }.unique()

fun <O, T : Comparable<O>> T.mapIsLessThan(other: Emitter<O>) =
        other.map { this < it }.unique()


fun <O, T : Comparable<O>> Emitter<T>.mapIsNotLessThan(other: Emitter<O>) =
        combineWith(other) { thisValue, otherValue -> thisValue >= otherValue }.unique()

fun <O, T : Comparable<O>> Emitter<T>.mapIsNotLessThan(other: O) =
        map { it >= other }.unique()

fun <O, T : Comparable<O>> T.mapIsNotLessThan(other: Emitter<O>) =
        other.map { this >= it }.unique()


fun <O, T : Comparable<O>> Emitter<T>.mapIsNotLargeThan(other: Emitter<O>) =
        combineWith(other) { thisValue, otherValue -> thisValue <= otherValue }.unique()

fun <O, T : Comparable<O>> Emitter<T>.mapIsNotLargeThan(other: O) =
        map { it <= other }.unique()

fun <O, T : Comparable<O>> T.mapIsNotLargeThan(other: Emitter<O>) =
        other.map { this <= it }.unique()

fun <T : Comparable<T>> max(
        firstSource: Emitter<T>,
        secondSource: Emitter<T>
) = Emitter
        .combine(
                firstSource = firstSource,
                secondSource = secondSource
        ) { first, second ->
            (first > second).checkBoolean(
                    ifTrue = { first },
                    ifFalse = { second }
            )
        }
        .unique()

fun <T : Comparable<T>> max(
        firstSource: Emitter<T>,
        second: T
) = firstSource
        .map { first ->
            (first > second).checkBoolean(
                    ifTrue = { first },
                    ifFalse = { second }
            )
        }
        .unique()

fun <T : Comparable<T>> max(first: T, secondSource: Emitter<T>) =
        max(secondSource, first)

fun <T : Comparable<T>> min(
        firstSource: Emitter<T>,
        secondSource: Emitter<T>
) = Emitter
        .combine(
                firstSource = firstSource,
                secondSource = secondSource
        ) { first, second ->
            (first < second).checkBoolean(
                    ifTrue = { first },
                    ifFalse = { second }
            )
        }
        .unique()

fun <T : Comparable<T>> min(
        firstSource: Emitter<T>,
        second: T
) = firstSource
        .map { first ->
            (first < second).checkBoolean(
                    ifTrue = { first },
                    ifFalse = { second }
            )
        }
        .unique()

fun <T : Comparable<T>> min(first: T, secondSource: Emitter<T>) =
        min(secondSource, first)