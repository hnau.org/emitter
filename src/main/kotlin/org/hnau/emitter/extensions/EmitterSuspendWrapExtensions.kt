package org.hnau.emitter.extensions

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.emitter.EmitterDetacher
import org.hnau.emitter.Emitter


class SuspendValueHandler<I, O>(
        value: I,
        coroutinesExecutor: (suspend CoroutineScope.() -> Unit) -> Job,
        action: suspend CoroutineScope.(value: I, onNewValue: (O) -> Unit) -> Unit,
        observer: (O) -> Unit
) {

    @Volatile
    private var active = true

    private val job = coroutinesExecutor {
        action(value) { result ->
            active.ifFalse { return@action }
            synchronized(this@coroutinesExecutor) {
                active.ifFalse { return@action }
                observer(result)
            }
        }
    }

    fun cancel() {
        active = false
        job.cancel()
    }

}

fun <I, O> Emitter<I>.suspendWrap(
        coroutinesExecutor: (suspend CoroutineScope.() -> Unit) -> Job,
        action: suspend CoroutineScope.(value: I, onNewValue: (O) -> Unit) -> Unit
) = Emitter.create<O> { observer ->

    object : () -> Unit {

        private var currentHandler: SuspendValueHandler<I, O>? = null
            @Synchronized
            set(value) {
                field?.cancel()
                field = value
            }

        private val thisEmitterDetacher = this@suspendWrap.observe { newValue ->
            currentHandler = SuspendValueHandler(
                    value = newValue,
                    coroutinesExecutor = coroutinesExecutor,
                    action = action,
                    observer = observer
            )
        }

        override fun invoke() {
            thisEmitterDetacher()
            currentHandler = null
        }

    }

}

inline fun <I, O> Emitter<I>.suspendMap(
        noinline coroutinesExecutor: (suspend CoroutineScope.() -> Unit) -> Job,
        crossinline converter: suspend CoroutineScope.(I) -> O
) = suspendWrap<I, O>(
        coroutinesExecutor = coroutinesExecutor
) { value, onNewValue ->
    val convertedValue = converter(value)
    onNewValue(convertedValue)
}