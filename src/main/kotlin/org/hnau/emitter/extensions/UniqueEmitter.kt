package org.hnau.emitter.extensions

import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.unsafe
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.PushEmitter
import org.hnau.emitter.utils.EmitterDetacherWrapper
import org.hnau.emitter.utils.observe


class UniqueEmitter<T>(
        private val source: Emitter<T>,
        private val comparator: (first: T, second: T) -> Boolean = { first, second -> first == second }
) : PushEmitter<T>() {

    private val value = Lateinit.unsafe<T>()

    private val detacherWrapper = EmitterDetacherWrapper()

    override fun tryGetValue(
            onValue: (T) -> Unit
    ) = value.checkPossible(
            ifValueExists = onValue,
            ifValueNotExists = {}
    )

    @Synchronized
    private fun onChanged(newValue: T) {
        value
                .checkPossible(
                        ifValueNotExists = { true },
                        ifValueExists = { oldValue ->
                            !comparator(oldValue, newValue)
                        }
                )
                .ifTrue {
                    value.set(newValue)
                    onChanged()
                }
    }

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        source.observe(detacherWrapper, ::onChanged)
    }

    override fun afterLastDetached() {
        detacherWrapper.detach()
        super.afterLastDetached()
    }

}


fun <T> Emitter<T>.unique(
        comparator: (first: T, second: T) -> Boolean = { first, second -> first == second }
): Emitter<T> = UniqueEmitter(
        source = this,
        comparator = comparator
)