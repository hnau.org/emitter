package org.hnau.emitter.extensions

import org.hnau.emitter.Emitter

fun <T, C : Collection<T>> Emitter<C>.callIfEmpty() = callIf(Collection<T>::isEmpty)
fun <T, C : Collection<T>> Emitter<C>.callIfNotEmpty() = callIf(Collection<T>::isNotEmpty)

fun <T, C : Collection<T>> Emitter<C>.filterEmpty() = filter(Collection<T>::isEmpty)
fun <T, C : Collection<T>> Emitter<C>.filterNotEmpty() = filter(Collection<T>::isNotEmpty)
fun <T, C : Collection<T>> Emitter<C>.mapIsEmpty() = map(Collection<T>::isEmpty).unique()
fun <T, C : Collection<T>> Emitter<C>.mapIsNotEmpty() = map(Collection<T>::isNotEmpty).unique()