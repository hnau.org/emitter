package org.hnau.emitter.extensions

import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.Emitter


inline fun <I, O> Emitter<I>.wrap(
        crossinline action: (value: I, onResult: (O) -> Unit) -> Unit
) = Emitter.create<O> { observer ->
    observe { value ->
        var observerLocal: ((O) -> Unit)? = observer
        action(value) { result ->
            observerLocal?.invoke(result)
        }
        observerLocal = null
    }
}

inline fun <I, O> Emitter<I>.map(
        crossinline converter: (I) -> O
) = wrap<I, O> { value, onResult ->
    onResult(converter(value))
}

inline fun <I, O> Emitter<I>.mapNotNull(
        crossinline converter: (I) -> O?
) = wrap<I, O> { value, onResult ->
    converter(value)?.let(onResult)
}

inline fun <T> Emitter<T>.filter(
        crossinline predicate: (T) -> Boolean
) = wrap<T, T> { value, onResult ->
    value.takeIf(predicate)?.let(onResult)
}

fun <T> Emitter<T>.mapToString() =
        map(Any?::toString)

fun <T> Emitter<T?>.filterNotNull() =
        wrap<T?, T> { value, onResult ->
            value?.let(onResult)
        }

inline fun <T> Emitter<T>.callIf(
        crossinline predicate: (T) -> Boolean
) = wrap<T, Unit> { value, onResult ->
    predicate(value).ifTrue { onResult(Unit) }
}
