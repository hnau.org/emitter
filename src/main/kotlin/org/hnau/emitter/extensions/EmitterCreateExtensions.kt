package org.hnau.emitter.extensions

import org.hnau.emitter.EmitterDetacher
import org.hnau.emitter.Emitter
import org.hnau.emitter.EmitterDetachers


inline fun <T> Emitter.Companion.create(
        crossinline onObserverAttached: ((T) -> Unit) -> EmitterDetacher
) = object : Emitter<T> {
    override fun observe(observer: (T) -> Unit) =
            onObserverAttached(observer)
}

inline fun <T> Emitter.Companion.simple(
        crossinline getter: () -> T
) = create<T> { observer ->
    observer(getter())
    EmitterDetachers.empty
}

fun <T> Emitter.Companion.single(value: T) =
        simple { value }

fun <T> T.toEmitter() =
        Emitter.single(this)

private val falseEmitter by lazy { false.toEmitter() }
val Emitter.Companion.alwaysFalse get() = falseEmitter

private val trueEmitter by lazy { true.toEmitter() }
val Emitter.Companion.alwaysTrue get() = trueEmitter

private val unitEmitter by lazy { Unit.toEmitter() }
val Emitter.Companion.alwaysUnit get() = unitEmitter

fun <T> Emitter.Companion.createAlwaysNull() =
        Emitter.single<T?>(null)