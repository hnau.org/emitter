package org.hnau.emitter.utils

import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.always.content.ManualEmitter
import java.util.concurrent.atomic.AtomicInteger


class InProgressRegistrator {

    private val inProgressInner = ManualEmitter(false)
    val inProgress: Emitter<Boolean> get() = inProgressInner

    private var blocksCount = 0
        set(blocksCount) {
            field = blocksCount
            val locked = blocksCount > 0
            (inProgressInner.value != locked)
                    .ifTrue { inProgressInner.value = locked }
        }

    private fun updateBlocksCount(
            delta: Int
    ) = synchronized(this) {
        blocksCount = (blocksCount + delta).coerceAtLeast(0)
    }

    @PublishedApi
    internal fun incBlocksCount() = updateBlocksCount(1)

    @PublishedApi
    internal fun decBlocksCount() = updateBlocksCount(-1)

    inline operator fun <R> invoke(
            block: () -> R
    ) = try {
        incBlocksCount()
        block()
    } finally {
        decBlocksCount()
    }

}