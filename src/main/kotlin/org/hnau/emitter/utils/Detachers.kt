package org.hnau.emitter.utils

import org.hnau.emitter.EmitterDetacher
import org.hnau.emitter.Emitter
import java.util.*


class EmitterDetachers {

    private val detachers = LinkedList<EmitterDetacher>()

    fun add(detacher: EmitterDetacher) {
        detachers.add(detacher)
    }

    fun detach() = synchronized(detachers) {
        detachers.forEach(EmitterDetacher::invoke)
        detachers.clear()
    }

}

fun <T> Emitter<T>.observe(detachers: EmitterDetachers, observer: (T) -> Unit) =
        detachers.add(observe(observer))