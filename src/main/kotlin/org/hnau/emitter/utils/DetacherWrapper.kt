package org.hnau.emitter.utils

import org.hnau.base.extensions.checkNullable
import org.hnau.emitter.EmitterDetacher
import org.hnau.emitter.Emitter


class EmitterDetacherWrapper {

    private var wrappedEmitterDetacher: EmitterDetacher? = null

    @Synchronized
    fun wrap(
            detacher: EmitterDetacher
    ) = wrappedEmitterDetacher.checkNullable(
            ifNull = {
                wrappedEmitterDetacher = detacher
            },
            ifNotNull = {
                throw IllegalStateException("EmitterDetacher already wrapped")
            }
    )

    @Synchronized
    fun detach() {
        wrappedEmitterDetacher?.invoke()
        wrappedEmitterDetacher = null
    }

}

fun <T> Emitter<T>.observe(detacherWrapper: EmitterDetacherWrapper, observer: (T) -> Unit) =
        detacherWrapper.wrap(observe(observer))