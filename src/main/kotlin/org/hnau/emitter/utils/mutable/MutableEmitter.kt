package org.hnau.emitter.utils.mutable

import org.hnau.emitter.Emitter


data class MutableEmitter<T>(
        val emitter: Emitter<T>,
        val setValue: (T) -> Unit
)