package org.hnau.emitter.utils.mutable

import org.hnau.emitter.Emitter
import org.hnau.emitter.observing.push.always.content.ManualEmitter


fun <T> Emitter<T>.toMutable(
        setValue: (T) -> Unit
) = MutableEmitter(
        emitter = this,
        setValue = setValue
)

fun <T> ManualEmitter<T>.toMutable() =
        toMutable { newValue -> value = newValue }