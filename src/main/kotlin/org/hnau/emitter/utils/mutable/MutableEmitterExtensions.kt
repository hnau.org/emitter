package org.hnau.emitter.utils.mutable

import org.hnau.base.data.mapper.Mapper
import org.hnau.emitter.extensions.map


fun <I, O> MutableEmitter<I>.map(
        mapper: Mapper<I, O>
) = MutableEmitter(
        emitter = emitter.map(mapper.direct),
        setValue = { newValue -> setValue(mapper.reverse(newValue)) }
)