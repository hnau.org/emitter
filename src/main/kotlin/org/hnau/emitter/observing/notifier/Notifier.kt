package org.hnau.emitter.observing.notifier

import org.hnau.emitter.observing.ObservingEmitter


abstract class Notifier : ObservingEmitter<Unit>() {

    protected open fun notifyObservers() = call(Unit)

}