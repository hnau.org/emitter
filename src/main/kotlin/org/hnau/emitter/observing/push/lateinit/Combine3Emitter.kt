package org.hnau.emitter.observing.push.lateinit

import org.hnau.base.data.possible.box.Box
import org.hnau.base.data.possible.box.unsafe
import org.hnau.emitter.Emitter
import org.hnau.emitter.utils.EmitterDetachers
import org.hnau.emitter.utils.observe


class Combine3Emitter<A, B, C, Z>(
        private val firstSource: Emitter<A>,
        private val secondSource: Emitter<B>,
        private val thirdSource: Emitter<C>,
        private val combinator: (A, B, C) -> Z
) : LateinitEmitter<Z>() {

    private val firstValue = Box.unsafe<A>()
    private val secondValue = Box.unsafe<B>()
    private val thirdValue = Box.unsafe<C>()

    private val detachers = EmitterDetachers()

    private fun setIfNeedUnsafe() = firstValue.checkPossible(
            ifValueExists = { first ->
                secondValue.checkPossible(
                        ifValueExists = { second ->
                            thirdValue.checkPossible(
                                    ifValueExists = { third -> set(combinator(first, second, third)) },
                                    ifValueNotExists = { }
                            )
                        },
                        ifValueNotExists = { }
                )
            },
            ifValueNotExists = { }
    )

    private inline fun synchronizeAndSetIfNeed(
            before: () -> Unit
    ) = synchronized(this) {
        before()
        setIfNeedUnsafe()
    }

    @Synchronized
    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        firstSource.observe(detachers) { newA ->
            synchronizeAndSetIfNeed { firstValue.set(newA) }
        }
        secondSource.observe(detachers) { newB ->
            synchronizeAndSetIfNeed { secondValue.set(newB) }
        }
        thirdSource.observe(detachers) { newC ->
            synchronizeAndSetIfNeed { thirdValue.set(newC) }
        }
    }

    @Synchronized
    override fun afterLastDetached() {
        super.afterLastDetached()
        detachers.detach()
    }

}

fun <A, B, C, Z> Emitter.Companion.combine(
        firstSource: Emitter<A>,
        secondSource: Emitter<B>,
        thirdSource: Emitter<C>,
        combinator: (A, B, C) -> Z
): Emitter<Z> = Combine3Emitter(
        firstSource = firstSource,
        secondSource = secondSource,
        thirdSource = thirdSource,
        combinator = combinator
)