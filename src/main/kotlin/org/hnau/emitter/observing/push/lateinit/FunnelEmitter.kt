package org.hnau.emitter.observing.push.lateinit

import org.hnau.emitter.Emitter
import org.hnau.emitter.utils.EmitterDetachers
import org.hnau.emitter.utils.observe


class FunnelEmitter<T>(
        private val sources: Iterable<Emitter<T>>
) : LateinitEmitter<T>() {

    private val detachers = EmitterDetachers()

    private val onSourceCall = this::set

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        sources.forEach { it.observe(detachers, onSourceCall) }
    }

    override fun afterLastDetached() {
        super.afterLastDetached()
        detachers.detach()
    }

}

fun <T> Iterable<Emitter<T>>.line(): Emitter<T> =
        FunnelEmitter(this)