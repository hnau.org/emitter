package org.hnau.emitter.observing.push.lateinit

import org.hnau.base.data.possible.lateinit.Lateinit
import org.hnau.base.data.possible.lateinit.synchronized
import org.hnau.emitter.observing.push.PushEmitter


abstract class LateinitEmitter<T>: PushEmitter<T>() {

    private val value = Lateinit.synchronized<T>()

    @Suppress("DEPRECATION")
    fun <R> checkContains(
            ifContains: (value: T) -> R,
            ifNotContains: () -> R
    ) = value.checkPossible(
            ifValueExists = ifContains,
            ifValueNotExists = ifNotContains
    )

    @Suppress("DEPRECATION")
    protected open fun set(value: T) {
        this.value.set(value)
        onChanged()
    }

    @Suppress("DEPRECATION")
    protected open fun setUnique(value: T) = synchronized(this) {
        this.value.checkPossible(
                ifValueExists = { (it == value).apply { this@LateinitEmitter.value.set(value) } },
                ifValueNotExists = { false }
        )
    }

    @Suppress("UNCHECKED_CAST")
    override final fun tryGetValue(onValue: (T) -> Unit) {
        checkContains(
                ifContains = onValue,
                ifNotContains = { }
        )
    }

}