package org.hnau.emitter.observing.push.always

import org.hnau.base.extensions.me
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.utils.EmitterDetachers
import org.hnau.emitter.utils.observe


class LineEmitter<T>(
        sources: Iterable<Emitter<T>>
) : AlwaysEmitter<List<T?>>() {

    private val sources = sources.map(Emitter<T>::me)

    private val size = this.sources.count()

    private val detachers = EmitterDetachers()

    private val values = run {
        val list = ArrayList<T?>()
        repeat(size) { list.add(null) }
        return@run list
    }

    override val value: List<T?> get() = values

    private var isAttachingToSources = false
    private var sourceEmittedWhenAttachingToSources = false

    private fun onSourceCall(
            i: Int,
            value: T
    ) = synchronized(this) {
        values[i] = value
        if (isAttachingToSources) {
            sourceEmittedWhenAttachingToSources = true
            return@synchronized
        }
        onChanged()
    }

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        synchronized(this) {
            isAttachingToSources = true
            sourceEmittedWhenAttachingToSources = false
        }
        sources.forEachIndexed { i, source ->
            source.observe(detachers) { value -> onSourceCall(i, value) }
        }
        synchronized(this) {
            isAttachingToSources = false
            if (sourceEmittedWhenAttachingToSources) {
                onChanged()
            }
        }
    }

    override fun afterLastDetached() {
        super.afterLastDetached()
        synchronized(this) {
            detachers.detach()
        }
    }

}

inline fun <T, R> Iterable<Emitter<T>>.line(
        crossinline action: (List<T?>) -> R
) = LineEmitter(this).map(action)