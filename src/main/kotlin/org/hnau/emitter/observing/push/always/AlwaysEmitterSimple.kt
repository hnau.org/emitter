package org.hnau.emitter.observing.push.always


class AlwaysEmitterSimple<T>(
        private val getValue: () -> T
) : AlwaysEmitter<T>() {

    override val value get() = getValue()

    override public fun onChanged() = super.onChanged()

}