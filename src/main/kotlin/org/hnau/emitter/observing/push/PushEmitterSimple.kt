package org.hnau.emitter.observing.push


class PushEmitterSimple<T>(
        private val tryGetValue: ((T) -> Unit) -> Unit
) : PushEmitter<T>() {

    override public fun onChanged() = super.onChanged()

    override fun tryGetValue(onValue: (T) -> Unit) = tryGetValue.invoke(onValue)

}