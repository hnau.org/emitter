package org.hnau.emitter.observing.push.lateinit

import org.hnau.emitter.Emitter
import org.hnau.emitter.utils.EmitterDetacherWrapper
import org.hnau.emitter.utils.observe


class CacheEmitter<T : Any>(
        private val source: Emitter<T>
) : LateinitEmitter<T>() {

    private val detacherWrapper = EmitterDetacherWrapper()

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        source.observe(detacherWrapper, this::set)
    }

    override fun afterLastDetached() {
        detacherWrapper.detach()
        super.afterLastDetached()
    }

}

fun <T : Any> Emitter<T>.cache(): Emitter<T> = CacheEmitter(
        source = this
)