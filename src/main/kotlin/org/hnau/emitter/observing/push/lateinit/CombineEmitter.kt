package org.hnau.emitter.observing.push.lateinit

import org.hnau.base.data.possible.box.Box
import org.hnau.base.data.possible.box.unsafe
import org.hnau.emitter.Emitter
import org.hnau.emitter.extensions.map
import org.hnau.emitter.utils.EmitterDetachers
import org.hnau.emitter.utils.observe


class CombineEmitter<A, B, Z>(
        private val firstSource: Emitter<A>,
        private val secondSource: Emitter<B>,
        private val combinator: (A, B) -> Z
) : LateinitEmitter<Z>() {

    private val firstValue = Box.unsafe<A>()
    private val secondValue = Box.unsafe<B>()

    private val detachers = EmitterDetachers()

    private fun setIfNeedUnsafe() = firstValue.checkPossible(
            ifValueExists = { first ->
                secondValue.checkPossible(
                        ifValueExists = { second -> set(combinator(first, second)) },
                        ifValueNotExists = { }
                )
            },
            ifValueNotExists = { }
    )

    private inline fun synchronizeAndSetIfNeed(
            before: () -> Unit
    ) = synchronized(this) {
        before()
        setIfNeedUnsafe()
    }

    @Synchronized
    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        firstSource.observe(detachers) { newA ->
            synchronizeAndSetIfNeed { firstValue.set(newA) }
        }
        secondSource.observe(detachers) { newB ->
            synchronizeAndSetIfNeed { secondValue.set(newB) }
        }
    }

    @Synchronized
    override fun afterLastDetached() {
        super.afterLastDetached()
        detachers.detach()
    }

}

inline fun <A, B, R> Emitter.Companion.combine(
        firstSource: Emitter<A>,
        secondSource: Emitter<B>,
        crossinline combinator: (A, B) -> R
) = firstSource
        .map { firstValue ->
            secondSource.map { secondValue ->
                combinator(firstValue, secondValue)
            }
        }
        .collapse()

fun <T, O, R> Emitter<T>.combineWith(
        other: Emitter<O>,
        combinator: (T, O) -> R
) = Emitter.combine(
        firstSource = this,
        secondSource = other,
        combinator = combinator
)