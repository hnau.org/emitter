package org.hnau.emitter.observing.push.always.content

import org.hnau.base.data.delegate.mutable.MutableDelegate


class ManualEmitter<T>(
        initialValue: T
) : ContentEmitter<T>(
        initialValue
), MutableDelegate<T> {

    override public var value: T
        set(value) {
            super.value = value
        }
        get() = super.value

    override fun get() = value

    override fun set(value: T) {
        this.value = value
    }

}