package org.hnau.emitter.observing.push.always

import org.hnau.emitter.observing.push.PushEmitter


abstract class AlwaysEmitter<T> : PushEmitter<T>() {

    protected abstract val value: T

    override final fun tryGetValue(onValue: (T) -> Unit) = onValue(value)

}