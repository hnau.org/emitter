package org.hnau.emitter.observing.push.always.content

import org.hnau.emitter.observing.push.always.AlwaysEmitter


abstract class ContentEmitter<T>(
        initialValue: T
) : AlwaysEmitter<T>() {

    override var value: T = initialValue
        protected set(value) {
            field = value
            onChanged()
        }

}

