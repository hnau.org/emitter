package org.hnau.emitter.observing.push

import org.hnau.emitter.observing.ObservingEmitter


abstract class PushEmitter<T> : ObservingEmitter<T>() {

    companion object {

        fun <T> create(
                tryGetValue: (onValue: (T) -> Unit) -> Unit
        ) = object : PushEmitter<T>() {
            override fun tryGetValue(onValue: (T) -> Unit) = tryGetValue(onValue)
        }

    }

    protected abstract fun tryGetValue(onValue: (T) -> Unit)

    protected open fun onChanged() =
            tryGetValue(this::call)

    override fun onAttached(observer: (T) -> Unit) {
        super.onAttached(observer)
        tryGetValue(observer)
    }

}