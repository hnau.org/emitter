package org.hnau.emitter.observing.push.lateinit

import org.hnau.emitter.EmitterDetacher
import org.hnau.emitter.Emitter
import org.hnau.emitter.utils.EmitterDetacherWrapper
import org.hnau.emitter.utils.observe


class CollapsingEmitter<T>(
        private val source: Emitter<Emitter<T>>
) : LateinitEmitter<T>() {

    private var sourceValueEmitterDetacher: EmitterDetacher? = null
        set(value) = synchronized(this) {
            field?.invoke()
            field = value
        }

    private val detacherWrapper = EmitterDetacherWrapper()

    override fun beforeFirstAttached() {
        super.beforeFirstAttached()
        source.observe(detacherWrapper) { sourceValue ->
            sourceValueEmitterDetacher = sourceValue.observe { set(it) }
        }
    }

    override fun afterLastDetached() {
        detacherWrapper.detach()
        sourceValueEmitterDetacher = null
        super.afterLastDetached()
    }

}

fun <T> Emitter<Emitter<T>>.collapse(): Emitter<T> =
        CollapsingEmitter(this)