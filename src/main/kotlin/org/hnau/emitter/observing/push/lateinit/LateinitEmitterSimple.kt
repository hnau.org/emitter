package org.hnau.emitter.observing.push.lateinit


class LateinitEmitterSimple<T> : LateinitEmitter<T>() {

    override public fun set(value: T) = super.set(value)
    override public fun setUnique(value: T) = super.setUnique(value)

}