package org.hnau.emitter.observing

import org.hnau.base.exception.ShouldNeverHappenException
import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue
import org.hnau.emitter.EmitterDetacher
import org.hnau.emitter.Emitter
import java.util.concurrent.CopyOnWriteArraySet


abstract class ObservingEmitter<T> : Emitter<T> {

    private val observers = CopyOnWriteArraySet<(T) -> Unit>()

    @Volatile
    private var isEmitting = false

    @Volatile
    private var valueToEmitExists = false
    private var valueToEmit: T? = null

    override fun observe(
            observer: (T) -> Unit
    ): EmitterDetacher {
        synchronized(observers) {
            observers.ifEmpty(this::beforeFirstAttached)
            observers.add(observer)
        }
        onAttached(observer)
        return { detach(observer) }
    }

    protected open fun call(value: T) {
        synchronized(this) {
            valueToEmit = value
            valueToEmitExists = true
            isEmitting.ifTrue { return }
            isEmitting = true
        }
        callObservers()
        isEmitting = false
    }

    private fun callObservers() {
        var interrupted: Boolean
        do {
            val value = synchronized(this) {
                valueToEmitExists.ifFalse { throw ShouldNeverHappenException }
                valueToEmitExists = false
                @Suppress("UNCHECKED_CAST")
                val result = valueToEmit as T
                valueToEmit = null
                result
            }
            interrupted = false
            val iterator = observers.iterator()
            while (iterator.hasNext() && !interrupted) {
                val observer = iterator.next()
                observer(value)
                valueToEmitExists.ifTrue { interrupted = true }
            }
        } while (interrupted)
    }


    protected open fun beforeFirstAttached() {}

    protected open fun onAttached(observer: (T) -> Unit) {}

    protected open fun onDetached(observer: (T) -> Unit) {}

    protected open fun afterLastDetached() {}


    private fun detach(
            observer: (T) -> Unit
    ) {
        val isEmpty = synchronized(observers) {
            observers.remove(observer).ifFalse { return }
            observers.isEmpty()
        }
        onDetached(observer)
        isEmpty.ifTrue(this::afterLastDetached)
    }

}