package org.hnau.emitter.observing


class ObservingEmitterSimple<T> : ObservingEmitter<T>() {

    public override fun call(value: T) = super.call(value)

}

fun ObservingEmitterSimple<Unit>.callListeners() = call(Unit)